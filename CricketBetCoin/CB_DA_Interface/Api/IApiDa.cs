﻿using CB_BaseObject;
using CB_BaseObject.CB;
using System;
using System.Collections.Generic;
using System.Text;
using static CB_BaseObject.PostModel.PostDataModel;

namespace CB_DA_Interface.Api
{
    public interface IApiDa
    {
        public ResultObject<string> forgotpass(string RequestType, signin data);

        ResultObject<Usersdetails> signin(string RequestType, signin data);


        ResultObject<Usersdetails> signup(string RequestType, Usersdetails data);

        ResultObject<Configuration> getConfiguration(string RequestType, string param);

        ResultObject<dynamic> Get_user_wallet_balance(string RequestType, Int64 Id);

        ResultObject<ViewBetDtails> addBet(string RequestType, Tbl_Bet_Details data);
  

        ResultObject<List<ViewBetDtails>> viewBet(string RequestType, string Id);
        ResultObject<List<MatchModel>> GethomeMatches(string RequestType, string Id, string param);
        ResultObject<List<Tbl_Odds_History>> GetMatch_odds(string RequestType, string Id);

        ResultObject<string> walletWithdraw(string RequestType, Tbl_Withdrawal_Request data);

        ResultObject<string> walletTopUp(string RequestType, WalletTopUp data);

        ResultObject<Wallet_Transaction> walletTransactionHistory(string RequestType, WalletTopUp data);
        ResultObject<string> updateemaillink(string RequestType, string email, string userid, string url);

        ResultObject<dynamic> matchDynamic(string RequestType, Int64 Id);

        ResultObject<List<Tbl_Quiz>> getQuiz(string RequestType, string Id, List<Tbl_Quiz> data);
    }
}
