#pragma checksum "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\Pages\Admin\ViewQuiz.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "fde3ed36487ade1dc6e5e6be6d36c91fdab6c212"
// <auto-generated/>
#pragma warning disable 1591
namespace CricketBet.Pages.Admin
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CricketBet;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CricketBet.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.Extensions.Configuration;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_Utilities.Enum;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_BaseObject;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_BaseObject.PostModel;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_BaseObject.CB;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_DA_Interface;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_DA;

#line default
#line hidden
#nullable disable
#nullable restore
#line 20 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_Mgr;

#line default
#line hidden
#nullable disable
#nullable restore
#line 21 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_Mgr_Interface;

#line default
#line hidden
#nullable disable
#nullable restore
#line 22 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_Mgr_Interface.Admin;

#line default
#line hidden
#nullable disable
#nullable restore
#line 23 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CricketBet.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 24 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CricketBet.Helpers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 25 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using System.IO;

#line default
#line hidden
#nullable disable
#nullable restore
#line 26 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CricketBet.Pages.Component;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\Pages\Admin\ViewQuiz.razor"
using CB_Utilities;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/quizlist")]
    public partial class ViewQuiz : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "div");
            __builder.AddAttribute(1, "class", "layout-px-spacing match-view");
            __builder.AddMarkupContent(2, "\r\n    ");
            __builder.OpenElement(3, "div");
            __builder.AddAttribute(4, "class", "row layout-top-spacing");
            __builder.AddMarkupContent(5, "\r\n        ");
            __builder.OpenElement(6, "div");
            __builder.AddAttribute(7, "class", "col-xl-12 col-lg-12 col-sm-12  layout-spacing");
            __builder.AddMarkupContent(8, "\r\n            ");
            __builder.OpenElement(9, "div");
            __builder.AddAttribute(10, "class", "widget-content widget-content-area br-6");
            __builder.AddMarkupContent(11, "\r\n                ");
            __builder.AddMarkupContent(12, @"<div class=""widget-header"">
                    <div class=""row"">
                        <div class=""col-xl-12 col-md-12 col-sm-12 col-12"">
                            <h4>View Quiz</h4>
                        </div>
                    </div>
                </div>
                ");
            __builder.OpenElement(13, "div");
            __builder.AddAttribute(14, "class", "widget-content widget-content-area");
            __builder.AddMarkupContent(15, "\r\n                    ");
            __builder.OpenElement(16, "div");
            __builder.AddAttribute(17, "class", "table-responsive mb-4");
            __builder.AddMarkupContent(18, "\r\n                        ");
            __builder.OpenElement(19, "table");
            __builder.AddAttribute(20, "id", "testmonialTable");
            __builder.AddAttribute(21, "class", "table style-3 table-hover");
            __builder.AddAttribute(22, "style", "text-transform:capitalize");
            __builder.AddMarkupContent(23, "\r\n                            ");
            __builder.AddMarkupContent(24, @"<thead>
                                <tr>
                                    <th>S.No</th>
                                    <th>Question Info</th>
                                    <th class=""text-right"">Status</th>
                                    <th class=""text-right"">Action</th>
                                </tr>
                            </thead>
                            ");
            __builder.OpenElement(25, "tbody");
            __builder.AddMarkupContent(26, "\r\n");
#nullable restore
#line 30 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\Pages\Admin\ViewQuiz.razor"
                                  
                                    int i = 1;
                                    foreach (Tbl_QuizAdmin list in resObject.ResultData)
                                    {

#line default
#line hidden
#nullable disable
            __builder.AddContent(27, "                                        ");
            __builder.OpenElement(28, "tr");
            __builder.AddMarkupContent(29, "\r\n                                            ");
            __builder.OpenElement(30, "td");
            __builder.AddContent(31, 
#nullable restore
#line 35 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\Pages\Admin\ViewQuiz.razor"
                                                 i

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(32, "\r\n                                            ");
            __builder.OpenElement(33, "td");
            __builder.AddMarkupContent(34, "\r\n                                                ");
            __builder.AddMarkupContent(35, "<b>Question-</b> ");
            __builder.AddContent(36, 
#nullable restore
#line 37 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\Pages\Admin\ViewQuiz.razor"
                                                                  list.question

#line default
#line hidden
#nullable disable
            );
            __builder.AddMarkupContent(37, "\r\n                                                <br>");
            __builder.AddMarkupContent(38, "<b>Date-</b> ");
            __builder.AddContent(39, 
#nullable restore
#line 38 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\Pages\Admin\ViewQuiz.razor"
                                                                    list.created_at.Value.Date.ToString("dd-MM-yyyy")

#line default
#line hidden
#nullable disable
            );
            __builder.AddMarkupContent(40, "\r\n                                            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(41, "\r\n\r\n");
#nullable restore
#line 41 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\Pages\Admin\ViewQuiz.razor"
                                             if (list.status == true)
                                            {

#line default
#line hidden
#nullable disable
            __builder.AddContent(42, "                                                ");
            __builder.OpenElement(43, "td");
            __builder.AddAttribute(44, "class", "text-right");
            __builder.OpenElement(45, "button");
            __builder.AddAttribute(46, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 43 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\Pages\Admin\ViewQuiz.razor"
                                                                                           ()=> editstatus(list.question_id,list.status)

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(47, "class", "shadow-none badge outline-badge-primary");
            __builder.AddAttribute(48, "style", "color:#8dbf42 !important; border: 1px solid #8dbf42 !important");
            __builder.AddContent(49, "Active");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(50, "\r\n");
#nullable restore
#line 44 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\Pages\Admin\ViewQuiz.razor"
                                            }
                                            else
                                            {

#line default
#line hidden
#nullable disable
            __builder.AddContent(51, "                                                ");
            __builder.OpenElement(52, "td");
            __builder.AddAttribute(53, "class", "text-right");
            __builder.OpenElement(54, "button");
            __builder.AddAttribute(55, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 47 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\Pages\Admin\ViewQuiz.razor"
                                                                                           ()=> editstatus(list.question_id,list.status)

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(56, "class", "badge outline-badge-danger shadow-none");
            __builder.AddContent(57, "Inactive");
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(58, "\r\n");
#nullable restore
#line 48 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\Pages\Admin\ViewQuiz.razor"
                                            }

#line default
#line hidden
#nullable disable
            __builder.AddContent(59, "                                            ");
            __builder.OpenElement(60, "td");
            __builder.AddAttribute(61, "class", "text-right");
            __builder.AddMarkupContent(62, "\r\n                                                ");
            __builder.OpenElement(63, "ul");
            __builder.AddAttribute(64, "class", "table-controls");
            __builder.AddMarkupContent(65, "\r\n\r\n");
#nullable restore
#line 52 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\Pages\Admin\ViewQuiz.razor"
                                                      
                                                        string encrypPass = Encryption.EncryptString(list.question_id.ToString(), Encryption.passkey);
                                                        encrypPass = encrypPass.Replace('+', '_');
                                                        encrypPass = encrypPass.Replace('/', '~');
                                                    

#line default
#line hidden
#nullable disable
            __builder.AddContent(66, "                                                    ");
            __builder.OpenElement(67, "li");
            __builder.AddAttribute(68, "id", "editButton");
            __builder.AddMarkupContent(69, "\r\n                                                        ");
            __builder.OpenElement(70, "a");
            __builder.AddAttribute(71, "href", "/quiz/" + (
#nullable restore
#line 58 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\Pages\Admin\ViewQuiz.razor"
                                                                        encrypPass

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(72, "class", "editButton");
            __builder.AddAttribute(73, "id", "cityid");
            __builder.AddAttribute(74, "data-placement", "top");
            __builder.AddAttribute(75, "title", "Edit");
            __builder.AddAttribute(76, "data-original-title", "Edit");
            __builder.AddMarkupContent(77, "\r\n                                                            ");
            __builder.AddMarkupContent(78, @"<svg xmlns=""http://www.w3.org/2000/svg"" width=""24"" height=""24"" viewBox=""0 0 24 24"" fill=""none"" stroke=""currentColor"" stroke-width=""2"" stroke-linecap=""round"" stroke-linejoin=""round"" class=""feather feather-edit-2 p-1 br-6 mb-1"">
                                                                <path d=""M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z""></path>
                                                            </svg>
                                                        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(79, "\r\n                                                    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(80, "\r\n                                                    ");
            __builder.OpenElement(81, "li");
            __builder.AddMarkupContent(82, "\r\n                                                        ");
            __builder.OpenElement(83, "a");
            __builder.AddAttribute(84, "onclick", Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 67 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\Pages\Admin\ViewQuiz.razor"
                                                                       () => delete(list.question_id.ToString())

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(85, "href", "javascript:void(0);");
            __builder.AddAttribute(86, "data-placement", "top");
            __builder.AddAttribute(87, "title", "Delete");
            __builder.AddAttribute(88, "data-original-title", "Delete");
            __builder.AddMarkupContent(89, "\r\n                                                            ");
            __builder.AddMarkupContent(90, @"<svg xmlns=""http://www.w3.org/2000/svg"" width=""24"" height=""24"" viewBox=""0 0 24 24"" fill=""none"" stroke=""currentColor"" stroke-width=""2"" stroke-linecap=""round"" stroke-linejoin=""round"" class=""feather feather-trash p-1 br-6 mb-1"">
                                                                <polyline points=""3 6 5 6 21 6""></polyline>
                                                                <path d=""M19 6v14a2 2 0 0 1-2 2H7a2 2 0 0 1-2-2V6m3 0V4a2 2 0 0 1 2-2h4a2 2 0 0 1 2 2v2"">
                                                                </path>
                                                            </svg>
                                                        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(91, "\r\n                                                    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(92, "\r\n                                                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(93, "\r\n                                            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(94, "\r\n                                        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(95, "\r\n");
#nullable restore
#line 82 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\Pages\Admin\ViewQuiz.razor"
                                        i++;
                                    }
                                

#line default
#line hidden
#nullable disable
            __builder.AddContent(96, "                            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(97, "\r\n                        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(98, "\r\n                    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(99, "\r\n                ");
            __builder.CloseElement();
            __builder.AddMarkupContent(100, "\r\n            ");
            __builder.CloseElement();
            __builder.AddMarkupContent(101, "\r\n        ");
            __builder.CloseElement();
            __builder.AddMarkupContent(102, "\r\n    ");
            __builder.CloseElement();
            __builder.AddMarkupContent(103, "\r\n");
            __builder.CloseElement();
        }
        #pragma warning restore 1998
#nullable restore
#line 94 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\Pages\Admin\ViewQuiz.razor"
       
    [Parameter] public string param { get; set; }
    ResultObject<List<Tbl_QuizAdmin>> resObject;
    Tbl_QuizAdmin _quizData;
    CBAdmin _CBAdmin;

    protected override void OnInitialized()
    {
        JSRuntime.InvokeVoidAsync("CSLoaderFadeIn");
        _CBAdmin = new CBAdmin(iConfig, httpContextAccessor, _IAdminMgr);
        resObject = _CBAdmin.quizMaster("viewqa", "");
        _quizData = new Tbl_QuizAdmin();
    }

    protected override void OnAfterRender(bool firstRender)
    {
        JSRuntime.InvokeVoidAsync("CSLoaderFadeOut");
        JSRuntime.InvokeVoidAsync("CSBindDataTableQA", "testmonialTable", false, 1, "ASC");
        JSRuntime.InvokeAsync<string>("setTitle", new object[] { "View Quiz" });
    }


    void editstatus(long id, bool? status)
    {
        ResultObject<string> res1 = new ResultObject<string>();
        res1 = _CBAdmin.editQuizMaster("activeqa", id.ToString(), _quizData);
        if (res1.ResultMessage == "Success")
        {
            JSRuntime.InvokeVoidAsync("CSAlertMsg", "Success", "Quiz Status updated successfully");
            JSRuntime.InvokeVoidAsync("CSBindDataTableQA", "testmonialTable", false, 1, "ASC");
            resObject = _CBAdmin.quizMaster("viewqa", "");
        }
        else
        {
            JSRuntime.InvokeVoidAsync("CSAlertMsg", "Error", "Some Error Occured");
        }
    }

    async void delete(string id)
    {
        bool confirmed = await JSRuntime.InvokeAsync<bool>("confirm", "Are you sure you want to delete the selected record?");
        if (confirmed)
        {
            ResultObject<string> res1 = new ResultObject<string>();
            res1 = _CBAdmin.editQuizMaster("deleteqa", id.ToString(), _quizData);
            if (res1.ResultMessage == "Success")
            {
                resObject = _CBAdmin.quizMaster("viewqa", "");
                await JSRuntime.InvokeVoidAsync("CSAlertMsg", "Success", "Quiz deleted successfully");
                await JSRuntime.InvokeVoidAsync("CSBindDataTableRemove", "testmonialTable");
                // await JSRuntime.InvokeVoidAsync("CSBindDataTableQA", "testmonialTable", false, 1, "ASC");
            }
            else
            {
                await JSRuntime.InvokeVoidAsync("CSAlertMsg", "Error", "Some Error Occured");
            }
            StateHasChanged();
        }
    }


#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IAdminMgr _IAdminMgr { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IHttpContextAccessor httpContextAccessor { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IConfiguration iConfig { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JSRuntime { get; set; }
    }
}
#pragma warning restore 1591
