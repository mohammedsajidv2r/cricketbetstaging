#pragma checksum "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\Pages\Admin\UpdateOdd.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "44a54a08257d6f0a7e4175b48cd77cbc6fedc98f"
// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace CricketBet.Pages.Admin
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CricketBet;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CricketBet.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.Extensions.Configuration;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_Utilities.Enum;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_BaseObject;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_BaseObject.PostModel;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_BaseObject.CB;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_DA_Interface;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_DA;

#line default
#line hidden
#nullable disable
#nullable restore
#line 20 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_Mgr;

#line default
#line hidden
#nullable disable
#nullable restore
#line 21 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_Mgr_Interface;

#line default
#line hidden
#nullable disable
#nullable restore
#line 22 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_Mgr_Interface.Admin;

#line default
#line hidden
#nullable disable
#nullable restore
#line 23 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CricketBet.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 24 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CricketBet.Helpers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 25 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using System.IO;

#line default
#line hidden
#nullable disable
#nullable restore
#line 26 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CricketBet.Pages.Component;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\Pages\Admin\UpdateOdd.razor"
using CB_Utilities;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/updateodds/{param}")]
    public partial class UpdateOdd : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 217 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\Pages\Admin\UpdateOdd.razor"
      

    [Parameter] public string param { get; set; }
    CBAdmin _CBAdmin;
    ResultObject<updateScore> resObject;
    updateScore _resModel;

    ResultObject<List<MatchData>> oddList;


    string Homeddl { get; set; }
    string Awayddl { get; set; }

    string enable1 = "enabled";
    string enable2 = "disabled";

    string timeformat = "";

    decimal? HomeTeam_odds;
    decimal? AwayTeam_odds;

    protected override void OnInitialized()
    {
        JSRuntime.InvokeVoidAsync("CSLoaderFadeIn");
        _CBAdmin = new CBAdmin(iConfig, httpContextAccessor, _IAdminMgr);
        if (param != null)
        {
            param = param.Replace('_', '+');
            param = param.Replace('~', '/');
            param = Encryption.DecryptString(param, Encryption.passkey);
            if (param == "\0")
            {
                navigationmanager.NavigateTo("404");
            }
        }
        resObject = _CBAdmin.viewScore("viewScore", param);
        if (resObject.ResultMessage != "Data Not Found.")
        {
            _resModel = resObject.ResultData;
            Homeddl = _resModel.batting.ToString();
            DateTime time = DateTime.Today.Add((TimeSpan)_resModel.match_timeStr);
            timeformat = time.ToString("hh:mm tt");
            oddList = _CBAdmin.viewMatch("viewScoreOdd", param);
            HomeTeam_odds = resObject.ResultData.HomeTeam_odds;
            AwayTeam_odds = resObject.ResultData.AwayTeam_odds;
        }
        else
        {
            _resModel = new updateScore();
            oddList = new ResultObject<List<MatchData>>();
        }
    }

    protected override void OnAfterRender(bool firstRender)
    {
        //if (firstRender)
        //{
        JSRuntime.InvokeVoidAsync("spinOdd");
        //}
        JSRuntime.InvokeVoidAsync("BindViewoddtexcel", "viewmatchdt", 0);
        JSRuntime.InvokeAsync<string>("setTitle", new object[] { "Update Odds" });
        JSRuntime.InvokeVoidAsync("CSLoaderFadeOut");

    }
    async void OnConfirm()
    {
        await JSRuntime.InvokeVoidAsync("CSLoaderFadeIn");
        ResultObject<string> res1 = new ResultObject<string>();
        _resModel.HomeTeam_odds = decimal.Parse(await JSRuntime.InvokeAsync<string>("spinValue", "oddsHome"));
        _resModel.AwayTeam_odds = decimal.Parse(await JSRuntime.InvokeAsync<string>("spinValue", "oddsAway"));

        //try
        //{
        //    _resModel.HomeTeam_odds = decimal.Parse(await JSRuntime.InvokeAsync<string>("spinValue", "oddsHome"));
        //    _resModel.AwayTeam_odds = decimal.Parse(await JSRuntime.InvokeAsync<string>("spinValue", "oddsAway"));
        //
        //}
        //catch (Exception ex)
        //{
        //    _resModel.HomeTeam_odds = (decimal)0.0;
        //    _resModel.AwayTeam_odds = (decimal)0.0;
        //}


        if ((_resModel.HomeTeam_odds == (decimal)0.0) && (_resModel.AwayTeam_odds == (decimal)0.0))
        {
            await JSRuntime.InvokeVoidAsync("CSAlertMsg", "Error", "Please enter Odds Value greater than 0");
        }
        //else if ((resObject.ResultData.HomeTeam_odds == HomeTeam_odds) && (resObject.ResultData.AwayTeam_odds == AwayTeam_odds))
        //{
        //    await JSRuntime.InvokeVoidAsync("CSAlertMsg", "Error", "Please update any odd value and then click Update button");
        //}
         else
         {
            res1 = _CBAdmin.updaeScore("updaeScoreOdd", _resModel);
            if (res1.ResultMessage == "Success")
            {
                await JSRuntime.InvokeVoidAsync("CSAlertMsg", "Success", "Match Odds updated successfully.");
                resObject = _CBAdmin.viewScore("viewScore", param);
                _resModel = resObject.ResultData;
                oddList = _CBAdmin.viewMatch("viewScoreOdd", param);
                await JSRuntime.InvokeVoidAsync("CSBindDataTableRemove", "viewmatchdt");
                HomeTeam_odds = resObject.ResultData.HomeTeam_odds;
                AwayTeam_odds = resObject.ResultData.AwayTeam_odds;
                StateHasChanged();
            }
            else
            {
                await JSRuntime.InvokeVoidAsync("CSAlertMsg", "Error", "Some Error Occured");
            }
        }
        await JSRuntime.InvokeVoidAsync("CSLoaderFadeOut");
    }

    protected void HandleInvalidSubmit()
    {

    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NavigationManager navigationmanager { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IAdminMgr _IAdminMgr { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IHttpContextAccessor httpContextAccessor { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IConfiguration iConfig { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JSRuntime { get; set; }
    }
}
#pragma warning restore 1591
