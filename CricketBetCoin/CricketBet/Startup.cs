using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Http;
using AutoMapper;
using CB_Mgr_Interface.Admin;
using CB_Mgr.Admin;
using CB_DA_Interface.Admin;
using CB_DA.Admin;
using CB_BaseObject;
using CB_BaseObject.CB;
using CricketBet.Helpers;
using CB_Mgr_Interface.Api;
using CB_Mgr.Api;
using CB_DA_Interface.Api;
using CB_DA.Api;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace CricketBet
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();

            services.AddTransient<IAdminMgr, AdminMgr>();
            services.AddTransient<IAdminDA, AdminDA>();

            services.AddTransient<IApiMgr, ApiMgr>();
            services.AddTransient<IApiDa, ApiDA>();

            services.AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            services.AddHttpContextAccessor();

            services.AddCors();
            services.AddControllers();

            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme).AddJwtBearer(
                options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuer = true,
                        ValidateAudience = true,
                        ValidateLifetime = true,
                        ValidateIssuerSigningKey = true,
                        ValidIssuer = Configuration["Jwt:Issuer"],
                        ValidAudience = Configuration["Jwt:Issuer"],
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(Configuration["Jwt:Key"]))
                    };
                });

            services.AddMvc();
            services.AddMvc().AddSessionStateTempDataProvider();

            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.IdleTimeout = TimeSpan.FromMinutes(60);
                options.Cookie.HttpOnly = true;
                // Make the session cookie essential
                options.Cookie.IsEssential = true;
            });
            services.AddSingleton<SessionState>();
            services.AddSingleton<SessionBootstrapper>();

            var config = new MapperConfiguration(cfg =>
          {
              cfg.AddProfile<MappingProfile>();
          });
            services.AddAutoMapper(typeof(Startup));

            //services.AddDateRangePicker(config =>
            //{
            //    config.Attributes = new Dictionary<string, object>
            //    {
            //        { "class", "form-control form-control-sm" }
            //    };
            //});
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/404");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            // app.UseCors(builder => builder.AllowAnyHeader().AllowAnyOrigin().AllowAnyMethod());

            // global cors policy
            app.UseCors(x => x
                .AllowAnyMethod()
                .AllowAnyHeader()
                .SetIsOriginAllowed(origin => true) // allow any origin
                .AllowCredentials()); // allow credentials

            app.UseSession();
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                 endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
        }

        public class MappingProfile : Profile
        {
            public MappingProfile()
            {
                CreateMap<ResultObject<Configuration>, ResultObject<Configuration>>();
                CreateMap<Configuration, Configuration>();

                CreateMap<ResultObject<List<Tbl_Configurations>>, ResultObject<List<Tbl_Configurations>>>();
                CreateMap<ResultObject<Tbl_Configurations>, ResultObject<Tbl_Configurations>>();
                CreateMap<Tbl_Configurations, Tbl_Configurations>();

                CreateMap<ResultObject<List<Tbl_Country_code>>, ResultObject<List<Tbl_Country_code>>>();
                CreateMap<ResultObject<Tbl_Country_code>, ResultObject<Tbl_Country_code>>();
                CreateMap<Tbl_Country_code, Tbl_Country_code>();

                CreateMap<ResultObject<string>, ResultObject<string>>();
                CreateMap<ResultObject<dynamic>, ResultObject<dynamic>>();
                CreateMap<ResultObject<List<dynamic>>, ResultObject<List<dynamic>>>();

                CreateMap<ResultObject<List<MatchModel>>, ResultObject<List<MatchModel>>>();
                CreateMap<ResultObject<MatchModel>, ResultObject<MatchModel>>();
                CreateMap<MatchModel, MatchModel>();
            }
        }
    }
}
