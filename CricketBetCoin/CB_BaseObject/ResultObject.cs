﻿using CB_Utilities.Enum;
using System;

namespace CB_BaseObject
{
    public class ResultObject<T>
    {
        public ResultType Result;
        public string ResultMessage;
        public string Remark;
        public T ResultData;
    }
}
