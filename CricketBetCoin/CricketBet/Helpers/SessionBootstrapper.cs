﻿using CB_BaseObject.CB;
using CB_Utilities;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CricketBet.Helpers
{
    public class SessionBootstrapper
    {
        private readonly IHttpContextAccessor accessor;
        private readonly SessionState session;
        public SessionBootstrapper(IHttpContextAccessor _accessor, SessionState _session)
        {
            accessor = _accessor;
            session = _session;
        }
        //public string SharedButtonLabel = "";

        public void Bootstrap()
        {
            //Singleton Item: services.AddSingleton<SessionState>(); in Startup.cs
            //Code to save data in server side session
            //If session already has data

            Tbl_Admin_User _authData = new Tbl_Admin_User();
            try
            {
                _authData = accessor.HttpContext.Session.GetObject<Tbl_Admin_User>("AuthLoginData");
            }
            catch (Exception ex)
            {

            }




            if (session.Items.ContainsKey("AuthLoginData") && _authData == null)
            {
                //get from singleton item
                _authData = session.Items["AuthLoginData"] as Tbl_Admin_User;
                // save to server side session
                accessor.HttpContext.Session.SetObject("AuthLoginData", _authData);
                //remove from singleton Item
                session.Items.Remove("AuthLoginData");
            }
            
            //If Session is not expired yet then  navigate to home
            if (accessor.HttpContext.Request.Path == "/logout")
            {
                accessor.HttpContext.Session.Clear();
                accessor.HttpContext.Response.Redirect("/");
            }
            if (_authData != null && accessor.HttpContext.Request.Path == "/")
            {
                accessor.HttpContext.Response.Redirect("/dashboard");
            }
            //If Session is expired then navigate to login
            else if (_authData == null && accessor.HttpContext.Request.Path != "/")
            {
                if (accessor.HttpContext.Request.Path.ToString().Contains("/resetpassword"))
                {
                }
                //else if (accessor.HttpContext.Request.Path.ToString().Contains("/verifyemail"))
                //{
                //}
                else
                {
                    accessor.HttpContext.Response.Redirect("/");
            
                }
            }
        }

        public Tbl_Admin_User checkLoginSession()
        {
            Tbl_Admin_User _authData = new Tbl_Admin_User();
            _authData = accessor.HttpContext.Session.GetObject<Tbl_Admin_User>("AuthLoginData");
            return _authData;
        }
    }
}
