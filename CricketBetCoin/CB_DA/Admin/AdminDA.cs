﻿using CB_BaseObject;
using CB_BaseObject.CB;
using CB_DA_Interface.Admin;
using CB_Utilities;
using CB_Utilities.Enum;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace CB_DA.Admin
{
    public class AdminDA : BaseConnection, IAdminDA
    {
        public AdminDA(IConfiguration config, IHttpContextAccessor httpContextAccessor) : base(config, httpContextAccessor)
        {

        }

        public ResultObject<Tbl_Admin_User> adminLoginAuth(string RequestType, Tbl_Admin_User data)
        {
            ResultObject<Tbl_Admin_User> resQuery = new ResultObject<Tbl_Admin_User>();
            try
            {
                //encrypt
                if(data.password != null)
                {
                    data.password = Encryption.EncryptString(data.password, Encryption.passkey);
                }
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@email", data.email);
                parameters.Add("@password", data.password);
                resQuery.ResultData = SqlMapper.Query<Tbl_Admin_User>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new Tbl_Admin_User();
                    resQuery.ResultMessage = "Data Not Found.";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<string> addMatch(string RequestType, MatchData data)
        {
            ResultObject<string> resQuery = new ResultObject<string>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                var XMLData = XmlConversion.SerializeToXElement(data);
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                parameters.Add("@id", data.match_id);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (_Result == "Success")
                {
                    resQuery.ResultData = _Result.ToString();
                    resQuery.Result = ResultType.Success;
                    resQuery.ResultMessage = "Success";
                }
                else
                {
                    resQuery.ResultMessage = "Error Occured";
                    resQuery.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "addMatch", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<Tbl_Tournaments>> viewTournament(string RequestType, string id)
        {
            ResultObject<List<Tbl_Tournaments>> resQuery = new ResultObject<List<Tbl_Tournaments>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", id);
                resQuery.ResultData = SqlMapper.Query<Tbl_Tournaments>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new List<Tbl_Tournaments>();
                    resQuery.ResultMessage = "Data Not Found";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "viewTournament", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<Tbl_Teams>> viewTeam(string RequestType, string id)
        {
            ResultObject<List<Tbl_Teams>> resQuery = new ResultObject<List<Tbl_Teams>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", id);
                resQuery.ResultData = SqlMapper.Query<Tbl_Teams>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new List<Tbl_Teams>();
                    resQuery.ResultMessage = "Data Not Found";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "viewTeam", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<MatchData>> viewMatch(string RequestType, string id)
        {
            ResultObject<List<MatchData>> resQuery = new ResultObject<List<MatchData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", id);
                resQuery.ResultData = SqlMapper.Query<MatchData>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new List<MatchData>();
                    resQuery.ResultMessage = "Data Not Found";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "viewMatch", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<updateScore> viewScore(string RequestType, string id)
        {
            ResultObject<updateScore> resQuery = new ResultObject<updateScore>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", id);
                resQuery.ResultData = SqlMapper.Query<updateScore>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new updateScore();
                    resQuery.ResultMessage = "Data Not Found.";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<string> updaeScore(string RequestType, updateScore data)
        {
            ResultObject<string> resQuery = new ResultObject<string>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                var XMLData = XmlConversion.SerializeToXElement(data);
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                parameters.Add("@id", data.match_id);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (_Result == "Success")
                {
                    resQuery.ResultData = _Result.ToString();
                    resQuery.Result = ResultType.Success;
                    resQuery.ResultMessage = "Success";
                }
                else
                {
                    resQuery.ResultMessage = "Error Occured";
                    resQuery.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "updaeScore", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<string> updateProfile(string RequestType, string param1, Tbl_Admin_User data)
        {
            ResultObject<string> resQuery = new ResultObject<string>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                var XMLData = XmlConversion.SerializeToXElement(data);
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (_Result == "Success")
                {
                    resQuery.ResultData = _Result.ToString();
                    resQuery.Result = ResultType.Success;
                    resQuery.ResultMessage = "Success";
                }
                else
                {
                    resQuery.ResultMessage = "Error Occured";
                    resQuery.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "updaeScore", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<Tbl_Users>> viewuser(string RequestType, string id)
        {
            ResultObject<List<Tbl_Users>> resQuery = new ResultObject<List<Tbl_Users>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", id);
                resQuery.ResultData = SqlMapper.Query<Tbl_Users>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new List<Tbl_Users>();
                    resQuery.ResultMessage = "Data Not Found";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "viewTournament", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<Tbl_Wallet_Transaction_Admin>> viewTransaction(string RequestType, string id)
        {
            ResultObject<List<Tbl_Wallet_Transaction_Admin>> resQuery = new ResultObject<List<Tbl_Wallet_Transaction_Admin>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", id);
                resQuery.ResultData = SqlMapper.Query<Tbl_Wallet_Transaction_Admin>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new List<Tbl_Wallet_Transaction_Admin>();
                    resQuery.ResultMessage = "Data Not Found";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "viewTransaction", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<string> walletTopUp(string RequestType, WalletTopUpAdd data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result != "0")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = "Failure";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<string> updatePassword(string RequestType, string param1, string param2, string param3)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                string _Result = "";
                if (RequestType == "passudpate")
                {
                    param1 = Encryption.EncryptString(param1, Encryption.passkey);
                    param2 = Encryption.EncryptString(param2, Encryption.passkey);
                    parameters.Add("@password", param1);
                    parameters.Add("@param", param2);
                    _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    if (_Result == "Success")
                    {
                        resPlan.ResultData = _Result.ToString();
                        resPlan.Result = ResultType.Success;
                        resPlan.ResultMessage = "Success";
                    }
                    else if (_Result == "The old password entered by you is incorrect")
                    {
                        resPlan.ResultData = _Result.ToString();
                        resPlan.Result = ResultType.Success;
                        resPlan.ResultMessage = _Result;
                    }
                    else
                    {
                        resPlan.ResultMessage = "Failure";
                        resPlan.Result = ResultType.Info;
                    }
                }
                else if (RequestType == "resetpassword")
                {
                    parameters.Add("@email", param1);
                    _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();

                    if (_Result != "0")
                    {
                        resPlan.ResultData = _Result.ToString();
                        resPlan.Result = ResultType.Success;
                        resPlan.ResultMessage = "Success";
                    }
                    else
                    {
                        resPlan.ResultMessage = "Erron an occured";
                        resPlan.Result = ResultType.Info;
                    }
                }
                else
                {
                    param1 = Encryption.EncryptString(param1, Encryption.passkey);
                    parameters.Add("@password", param1);
                    parameters.Add("@param", param2);
                    parameters.Add("@id", param3);
                    _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                    if (RequestType == "resetpasswordUpdate")
                    {
                        if (_Result == "Success")
                        {
                            resPlan.ResultData = _Result.ToString();
                            resPlan.Result = ResultType.Success;
                            resPlan.ResultMessage = "Success";
                        }
                        else
                        {
                            resPlan.ResultMessage = "some error Occured";
                            resPlan.Result = ResultType.Info;
                        }
                    }
                    else
                    {
                        if (_Result == "1")
                        {
                            resPlan.ResultData = _Result.ToString();
                            resPlan.Result = ResultType.Success;
                            resPlan.ResultMessage = "Success";
                        }
                        else if (_Result == "0")
                        {
                            resPlan.ResultData = _Result.ToString();
                            resPlan.Result = ResultType.Success;
                            resPlan.ResultMessage = "Success";
                        }
                        else
                        {
                            resPlan.ResultData = _Result.ToString();
                            resPlan.ResultMessage = "some error Occured";
                            resPlan.Result = ResultType.Info;
                        }
                    }

                }

            }
            catch (Exception ex)
            {
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<List<ViewBetDtailsAdmin>> viewBet(string RequestType, string Id)
        {
            ResultObject<List<ViewBetDtailsAdmin>> resQuery = new ResultObject<List<ViewBetDtailsAdmin>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", Id);
                resQuery.ResultData = SqlMapper.Query<ViewBetDtailsAdmin>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new List<ViewBetDtailsAdmin>();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "viewBet", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<Tbl_Withdrawal_RequestAdmin>> viewWithdrawal(string RequestType, string Id)
        {
            ResultObject<List<Tbl_Withdrawal_RequestAdmin>> resQuery = new ResultObject<List<Tbl_Withdrawal_RequestAdmin>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", Id);
                resQuery.ResultData = SqlMapper.Query<Tbl_Withdrawal_RequestAdmin>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new List<Tbl_Withdrawal_RequestAdmin>();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "viewBet", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<string> updateWirhdrawal(string RequestType, Tbl_Withdrawal_RequestAdmin data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = "Failure";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<reconcile_amountAdmin> reconcile_amount(string RequestType, string id, string param)
        {
            ResultObject<reconcile_amountAdmin> resQuery = new ResultObject<reconcile_amountAdmin>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", id);
                parameters.Add("@param", param);
                resQuery.ResultData = SqlMapper.Query<reconcile_amountAdmin>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();

                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new reconcile_amountAdmin();
                    resQuery.ResultMessage = "Data Not Found.";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<MatchData>> viewMatch(string RequestType, string id, string param, string data, string to)
        {
            ResultObject<List<MatchData>> resQuery = new ResultObject<List<MatchData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", id);
                parameters.Add("@param", param);
                parameters.Add("@fromdate", data);
                parameters.Add("@todate", to);
                resQuery.ResultData = SqlMapper.Query<MatchData>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new List<MatchData>();
                    resQuery.ResultMessage = "Data Not Found";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "viewMatch", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<FaqData>> viewfaq(string RequestType, string param1, string param2)
        {
            ResultObject<List<FaqData>> resPlan = new ResultObject<List<FaqData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", param1);
                resPlan.ResultData = SqlMapper.Query<FaqData>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resPlan.ResultData == null || resPlan.ResultData.Count <= 0)
                {
                    resPlan.ResultData = new List<FaqData>();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "viewfaq", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<string> editfaq(string RequestType, string param, FaqData data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                parameters.Add("@id", param);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else if (_Result == "Faq question Already Exists")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = _Result;
                }
                else
                {
                    resPlan.ResultMessage = "Erron an occured";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editfaq", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }


        public ResultObject<string> editCmsMaster(string RequestType, string param, CmsPageData data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                parameters.Add("@id", param);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else if (_Result == "CMS Title Already Exists")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = _Result;
                }
                else
                {
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editTagMaster", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<List<CmsPageData>> cmsMaster(string RequestType, string param1, string param2)
        {
            ResultObject<List<CmsPageData>> resPlan = new ResultObject<List<CmsPageData>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@param", param1);
                resPlan.ResultData = SqlMapper.Query<CmsPageData>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resPlan.ResultData == null || resPlan.ResultData.Count <= 0)
                {
                    resPlan.ResultData = new List<CmsPageData>();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "cmsMaster", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<List<Tbl_QuizAdmin>> quizMaster(string RequestType, string param1, string param2)
        {
            ResultObject<List<Tbl_QuizAdmin>> resPlan = new ResultObject<List<Tbl_QuizAdmin>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@param", param1);
                resPlan.ResultData = SqlMapper.Query<Tbl_QuizAdmin>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).ToList();

                if (resPlan.ResultData == null || resPlan.ResultData.Count <= 0)
                {
                    resPlan.ResultData = new List<Tbl_QuizAdmin>();
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
                else
                {
                    resPlan.ResultMessage = "Success";
                    resPlan.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "cmsMaster", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<string> editQuizMaster(string RequestType, string param, Tbl_QuizAdmin data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                parameters.Add("@id", param);
                string _Result = SqlMapper.Query<string>(con, "USP_Admin", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else if (_Result == "Question Already Exists")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = _Result;
                }
                else
                {
                    resPlan.ResultMessage = "Data Not Found.";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editTagMaster", "AdminDA", 0, ex.Message.ToString());
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }
    }
}
