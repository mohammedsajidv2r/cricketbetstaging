#pragma checksum "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\Pages\Admin\Withdrawal.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "d581fd2836de03b959fc1c0d050938340ba7f8b9"
// <auto-generated/>
#pragma warning disable 1591
#pragma warning disable 0414
#pragma warning disable 0649
#pragma warning disable 0169

namespace CricketBet.Pages.Admin
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.AspNetCore.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.AspNetCore.Components.Authorization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CricketBet;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CricketBet.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.AspNetCore.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using Microsoft.Extensions.Configuration;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_Utilities.Enum;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_BaseObject;

#line default
#line hidden
#nullable disable
#nullable restore
#line 16 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_BaseObject.PostModel;

#line default
#line hidden
#nullable disable
#nullable restore
#line 17 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_BaseObject.CB;

#line default
#line hidden
#nullable disable
#nullable restore
#line 18 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_DA_Interface;

#line default
#line hidden
#nullable disable
#nullable restore
#line 19 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_DA;

#line default
#line hidden
#nullable disable
#nullable restore
#line 20 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_Mgr;

#line default
#line hidden
#nullable disable
#nullable restore
#line 21 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_Mgr_Interface;

#line default
#line hidden
#nullable disable
#nullable restore
#line 22 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CB_Mgr_Interface.Admin;

#line default
#line hidden
#nullable disable
#nullable restore
#line 23 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CricketBet.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 24 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CricketBet.Helpers;

#line default
#line hidden
#nullable disable
#nullable restore
#line 25 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using System.IO;

#line default
#line hidden
#nullable disable
#nullable restore
#line 26 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\_Imports.razor"
using CricketBet.Pages.Component;

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/withdrawalxyz")]
    public partial class Withdrawal : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
        }
        #pragma warning restore 1998
#nullable restore
#line 93 "G:\project\v2r\CricketBetProject\CricketBetCoin\CricketBet\Pages\Admin\Withdrawal.razor"
       
    [Parameter] public string param { get; set; }
    ResultObject<List<Tbl_Withdrawal_RequestAdmin>> resObject;
    Tbl_Withdrawal_RequestAdmin _withdrawData;
    CBAdmin _CBAdmin;
    bool showingDialog = false;

    protected override void OnInitialized()
    {
        JSRuntime.InvokeVoidAsync("CSLoaderFadeIn");
        _CBAdmin = new CBAdmin(iConfig, httpContextAccessor, _IAdminMgr);
        resObject = _CBAdmin.viewWithdrawal("viewWihdrawal", "");
        _withdrawData = new Tbl_Withdrawal_RequestAdmin();
    }

    protected override void OnAfterRender(bool firstRender)
    {
        JSRuntime.InvokeVoidAsync("CSLoaderFadeOut");
        JSRuntime.InvokeVoidAsync("BindViewwtihdrawdtexcel", "viewwithdt", 0);
        JSRuntime.InvokeAsync<string>("setTitle", new object[] { "View Withdrawal Request" });

    }

    void Showwithdraw(long id)
    {
        _withdrawData = resObject.ResultData.Where(x => x.withdrawal_id == id).FirstOrDefault();
        showingDialog = true;
    }

    void CancelConfigure()
    {
        _withdrawData = new Tbl_Withdrawal_RequestAdmin();
        JSRuntime.InvokeVoidAsync("CSHideModel", "withdrawalpopup");
        showingDialog = false;
    }

    public void editstatus(long id, int status)
    {
        JSRuntime.InvokeVoidAsync("CSLoaderFadeIn");
        ResultObject<string> res1 = new ResultObject<string>();
        if (status == 1)
        {
            _withdrawData.approve_status = 1;
            _withdrawData.withdrawal_id = id;
        }
        res1 = _CBAdmin.updateWirhdrawal("withdrawalupdate", _withdrawData);
        if (res1.ResultMessage == "Success")
        {
            JSRuntime.InvokeVoidAsync("CSAlertMsg", "Success", "Approve updated successfully.");
            System.Threading.Thread.Sleep(2000);
            JSRuntime.InvokeVoidAsync("CSReloadFunc");
            //_withdrawData = new Tbl_Withdrawal_RequestAdmin();
            //resObject = _CBAdmin.viewWithdrawal("viewWihdrawal", "");
            //JSRuntime.InvokeVoidAsync("CSBindDataTableRemove", "viewwithdt");
        }
        else
        {
            JSRuntime.InvokeVoidAsync("CSAlertMsg", "Error", "Some Error Occured");
        }
        //StateHasChanged();
        JSRuntime.InvokeVoidAsync("CSLoaderFadeOut");

    }

    void ConfirmConfigure()
    {
        JSRuntime.InvokeVoidAsync("CSLoaderFadeIn");
        ResultObject<string> res1 = new ResultObject<string>();
        if (_withdrawData.approve_status == 1)
        {
            _withdrawData.remarks = "";
        }
        res1 = _CBAdmin.updateWirhdrawal("withdrawalupdate", _withdrawData);
        if (res1.ResultMessage == "Success")
        {
            JSRuntime.InvokeVoidAsync("CSAlertMsg", "Success", "Reject updated successfully.");
            System.Threading.Thread.Sleep(2000);
            JSRuntime.InvokeVoidAsync("CSReloadFunc");
            JSRuntime.InvokeVoidAsync("CSBindDataTableRemove", "viewwithdt");
            _withdrawData = new Tbl_Withdrawal_RequestAdmin();
            resObject = _CBAdmin.viewWithdrawal("viewWihdrawal", "");
        }
        else
        {
            JSRuntime.InvokeVoidAsync("CSAlertMsg", "Error", "Some Error Occured");
        }
        JSRuntime.InvokeVoidAsync("CSHideModel", "withdrawalpopup");
        showingDialog = false;
        // JSRuntime.InvokeVoidAsync("CSReloadFunc");
        JSRuntime.InvokeVoidAsync("CSLoaderFadeOut");
    }

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IAdminMgr _IAdminMgr { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IHttpContextAccessor httpContextAccessor { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IConfiguration iConfig { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private IJSRuntime JSRuntime { get; set; }
    }
}
#pragma warning restore 1591
