﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using CB_BaseObject;
using CB_BaseObject.CB;
using CB_Mgr_Interface.Admin;
using CB_Mgr_Interface.Api;
using CB_Utilities;
using CB_Utilities.Enum;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.JsonWebTokens;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using static CB_BaseObject.PostModel.PostDataModel;

namespace CricketBet.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    //[EnableCors("AllowOrigin")]
    public class CricketBetPostController : ControllerBase
    {
        public static IWebHostEnvironment _environment;
        IConfiguration _iConfig;
        IHttpContextAccessor _httpContextAccessor;
        IAdminMgr _IAdminMgr;
        IApiMgr _IApiMgr;


        public CricketBetPostController(IAdminMgr IAdminMgr, IApiMgr IApiMgr, IConfiguration iConfig, IHttpContextAccessor httpContextAccessor, IWebHostEnvironment environment)
        {
            _environment = environment;
            _iConfig = iConfig;
            _httpContextAccessor = httpContextAccessor;
            _IAdminMgr = IAdminMgr;
            _IApiMgr = IApiMgr;
        }

        public class FIleUploadAPI
        {
            public IFormFile files
            {
                get;
                set;
            }
        }

        #region userapi
        [HttpPost]
        public dynamic signIn(EncryptPostData encryptdata)
        //public dynamic signIn(signin data)
        {
            ResultObject<Usersdetails> res = new ResultObject<Usersdetails>();
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                signin data = JsonConvert.DeserializeObject<signin>(decryptdata);
                res = _IApiMgr.signin("signin", data);
                if (res.Result == ResultType.Success)
                {
                    //var tokenStr = GenerateJSONWebToken(res.ResultData); 
                    string otpmessage = res.ResultData.otp + " is the OTP to login into your Cricket24.Bet account.";
                    SendSms.snsSned(data.country_code + res.ResultData.mobile, otpmessage);
                    return new
                    {
                        result = 1,
                        message = "Success"
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Mobile number does not exist",
                        //data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                    data = ""
                } as dynamic;
            }
        }

        [HttpPost]
        public dynamic otpVerification(EncryptPostData encryptdata)
        //public dynamic otpVerification(signin data)
        {
            ResultObject<Usersdetails> res = new ResultObject<Usersdetails>();
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                signin data = JsonConvert.DeserializeObject<signin>(decryptdata);
                res = _IApiMgr.signin("otpverify", data);
                if (res.Result == ResultType.Success)
                {
                    ResultObject<dynamic> resWallet = new ResultObject<dynamic>();
                    resWallet = _IApiMgr.Get_user_wallet_balance("userwalletbal", res.ResultData.user_id);
                    var tokenStr = GenerateJSONWebToken(res.ResultData);
                    //if ((res.ResultData.email != "" && res.ResultData.email != null) && (res.ResultData.email_is_verified == 0 || res.ResultData.email_is_verified == null))
                    //{
                    //    verifyEmailsent(Convert.ToString(res.ResultData.user_id), (res.ResultData.first_name + " " + res.ResultData.last_name), res.ResultData.email);
                    //}
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = new
                        {
                            token = tokenStr,
                            res.ResultData.country_code_id,
                            res.ResultData.mobile,
                            res.ResultData.first_name,
                            res.ResultData.last_name,
                            res.ResultData.email,
                            res.ResultData.profile_picture,
                            res.ResultData.email_is_verified,
                            res.ResultData.status,
                            res.ResultData.last_login,
                            res.ResultData.created_at,
                            res.ResultData.updated_at,
                            walletBalance = Convert.ToDecimal(resWallet.ResultData.balance),
                            creditedCoin = Convert.ToInt32(res.ResultData.creditedCoin),
                            creditedCoinReferral = Convert.ToInt32(res.ResultData.creditedCoinReferral),
                            //res.ResultData.creditedCoin,
                            res.ResultData.referral_code,
                            res.ResultData.creditType,
                            //res.ResultData.creditedCoinReferral
                        }
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Incorrect verification code.",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        [HttpPost]
        public dynamic signUp(EncryptPostData encryptdata)
        //public dynamic signUp(Usersdetails data)
        {
            ResultObject<Usersdetails> res = new ResultObject<Usersdetails>();
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                Usersdetails data = JsonConvert.DeserializeObject<Usersdetails>(decryptdata);
                res = _IApiMgr.signup("signup", data);
                if (res.Result == ResultType.Success)
                {
                    string otpmessage = res.ResultData.otp + " is the OTP to login into your Cricket24.Bet account.";
                    SendSms.snsSned(res.ResultData.country_code + res.ResultData.mobile, otpmessage);
                    return new
                    {
                        result = 1,
                        message = "Success",
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Mobile number already exist",
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure"
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure"
                } as dynamic;
            }
        }

        [HttpPost]
        public dynamic referralVerification(EncryptPostData encryptdata)
        //public dynamic referralVerification(Usersdetails data)
        {
            ResultObject<string> res = new ResultObject<string>();

            string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
            Usersdetails data = JsonConvert.DeserializeObject<Usersdetails>(decryptdata);

            res = _IApiMgr.updateemaillink("referralVerify", "", "", data.referral_code);
            if (res.Result == ResultType.Success)
            {
                return new
                {
                    result = 1,
                    message = "Referral Code is Match",
                } as dynamic;
            }
            else
            {
                return new
                {
                    result = 0,
                    message = "Referral code does not match.",
                } as dynamic;
            }

        }


        [Authorize]
        [HttpPost]
        public dynamic forceLogin()
        {
            ResultObject<Usersdetails> res = new ResultObject<Usersdetails>();
            try
            {
                Usersdetails data = new Usersdetails();
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                data.user_id = long.Parse(userid);
                res = _IApiMgr.signup("forcelogin", data);
                if (res.Result == ResultType.Success)
                {
                    if (res.ResultData.last_login != null)
                    {
                        return new
                        {
                            result = 1,
                            message = "Success",
                            data = new
                            {
                                last_login = res.ResultData.last_login.Value.ToString("dd-MM-yyyy")
                            }
                        } as dynamic;
                    }
                    else
                    {
                        return new
                        {
                            result = 1,
                            message = "Success",
                            data = new
                            {
                                last_login = res.ResultData.last_login
                            }
                        } as dynamic;
                    }

                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }


        [Authorize]
        [HttpPost]
        public dynamic dailyReward()
        {
            ResultObject<Usersdetails> res = new ResultObject<Usersdetails>();
            try
            {
                Usersdetails data = new Usersdetails();
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                data.user_id = long.Parse(userid);
                res = _IApiMgr.signup("dailyReward", data);
                if (res.Result == ResultType.Success)
                {
                    ResultObject<dynamic> resWallet = new ResultObject<dynamic>();
                    resWallet = _IApiMgr.Get_user_wallet_balance("userwalletbal", res.ResultData.user_id);

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = new
                        {
                            //last_login = res.ResultData.last_login.Value.ToString("dd-MM-yyyy"),
                            last_login = res.ResultData.last_login,
                            res.ResultData.creditedCoin,
                            walletBalance = Convert.ToDecimal(resWallet.ResultData.balance)
                        }
                    } as dynamic;

                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "User Already got Reward",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        public dynamic myProfile()
        {
            ResultObject<Usersdetails> res = new ResultObject<Usersdetails>();
            try
            {
                Usersdetails data = new Usersdetails();
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                data.user_id = long.Parse(userid);
                res = _IApiMgr.signup("myProfile", data);
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = new
                        {
                            res.ResultData.country_code_id,
                            res.ResultData.country_code,
                            res.ResultData.mobile,
                            res.ResultData.first_name,
                            res.ResultData.last_name,
                            res.ResultData.email,
                            res.ResultData.profile_picture,
                            res.ResultData.email_is_verified,
                            res.ResultData.status,
                            res.ResultData.last_login,
                            res.ResultData.created_at,
                            res.ResultData.updated_at
                        }
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        public dynamic editProfile(EncryptPostData encryptdata)
        //public dynamic editProfile(Usersdetails data)
        {
            ResultObject<Usersdetails> res = new ResultObject<Usersdetails>();
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                Usersdetails data = JsonConvert.DeserializeObject<Usersdetails>(decryptdata);

                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                data.user_id = long.Parse(userid);
                res = _IApiMgr.signup("editProfile", data);
                if (res.Result == ResultType.Success)
                {
                    //if (res.ResultData.email != data.email)
                    //if (res.ResultData.email != null && res.ResultData.email != "")
                    if (res.ResultData.email_is_verified != 1 && res.ResultData.email != null && res.ResultData.email_link == null)
                    //if ((res.ResultData.email != "" && res.ResultData.email != null) && (res.ResultData.email_is_verified == 0 || res.ResultData.email_is_verified == null))
                    {
                        verifyEmailsent(Convert.ToString(res.ResultData.user_id), (res.ResultData.first_name + " " + res.ResultData.last_name), res.ResultData.email);
                    }

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = new
                        {
                            res.ResultData.country_code_id,
                            res.ResultData.mobile,
                            res.ResultData.first_name,
                            res.ResultData.last_name,
                            res.ResultData.email,
                            res.ResultData.profile_picture,
                            res.ResultData.email_is_verified,
                            res.ResultData.status,
                            res.ResultData.last_login,
                            res.ResultData.created_at,
                            res.ResultData.updated_at
                        }
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        #endregion

        [Authorize]
        [HttpPost]
        public dynamic getUserWalletBalance()
        {
            ResultObject<dynamic> res = new ResultObject<dynamic>();
            try
            {
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                res = _IApiMgr.Get_user_wallet_balance("userwalletbal", long.Parse(userid));
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData.balance,
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        //public dynamic addBet(Tbl_Bet_Details data)
        public dynamic addBet(EncryptPostData encryptdata)
        {
            ResultObject<ViewBetDtails> res = new ResultObject<ViewBetDtails>();
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                Tbl_Bet_Details data = JsonConvert.DeserializeObject<Tbl_Bet_Details>(decryptdata);

                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                data.user_id = long.Parse(userid);
                res = _IApiMgr.addBet("addBet", data);
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                    data = ""
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        public dynamic viewMyBet()
        {
            ResultObject<List<ViewBetDtails>> res = new ResultObject<List<ViewBetDtails>>();
            try
            {
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                res = _IApiMgr.viewBet("viewBet", userid.ToString());
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = new
                        {
                            live = res.ResultData.Where(x => x.won_status == 0).OrderByDescending(r => r.created_at).ToList(),
                            settled = res.ResultData.Where(x => x.won_status != 0).OrderByDescending(r => r.created_at).ToList(),
                        }
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                    data = ""
                } as dynamic;
            }
        }

        //[Authorize]
        [HttpPost]
        public dynamic getHomeMatches()
        {
            ResultObject<List<MatchModel>> res = new ResultObject<List<MatchModel>>();
            try
            {
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                string userid = null;
                if (claim.Count > 0)
                {
                    userid = claim[0].Value;
                }

                if (userid == null)
                {
                    res = _IApiMgr.GethomeMatches("gethome_matches", "", "");
                }
                else
                {
                    res = _IApiMgr.GethomeMatches("gethome_matches_userId", "", userid);
                }

                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = new
                        {
                            live = res.ResultData.Where(r => r.match_status == 1 && r.match_date.Value.Date == DateTime.Now.Date).OrderBy(r => r.match_date).ThenBy(r => r.match_time).ToList(),
                            upcomming = res.ResultData.Where(r => r.match_status != 1 && r.match_date.Value.Date >= DateTime.Now.Date).OrderBy(r => r.match_date).ThenBy(r => r.match_time).ToList(),

                        }
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        //[Authorize]
        [HttpPost]
        //public dynamic getMatches(Match_para Match_para)
        public dynamic getMatches(EncryptPostData encryptdata)
        {
            ResultObject<List<MatchModel>> res = new ResultObject<List<MatchModel>>();
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                Match_para Match_para = JsonConvert.DeserializeObject<Match_para>(decryptdata);

                res = _IApiMgr.GethomeMatches("gethome_matches", "", "");
                if (res.Result == ResultType.Success)
                {
                    if (Match_para.Is_Live == 1)
                    {
                        return new
                        {
                            result = 1,
                            message = "Success",
                            data = new
                            {
                                live = res.ResultData.Where(r => r.match_status == 1 && r.match_date.Value.Date == DateTime.Now.Date).OrderBy(r => r.match_date).ThenBy(r => r.match_time).ToList(),
                            }
                        } as dynamic;
                    }
                    else
                    {
                        return new
                        {
                            result = 1,
                            message = "Success",
                            data = new
                            {
                                upcomming = res.ResultData.Where(r => r.match_status != 1 && r.match_date.Value.Date >= DateTime.Now.Date).OrderBy(r => r.match_date).ThenBy(r => r.match_time).ToList(),
                            }
                        } as dynamic;
                    }
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        //[Authorize]
        [HttpPost]
        public dynamic getMatchOdds(EncryptPostData encryptdata)
        //public dynamic getMatchOdds(Match_para Match_para)
        {
            ResultObject<List<Tbl_Odds_History>> res = new ResultObject<List<Tbl_Odds_History>>();
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                Match_para Match_para = JsonConvert.DeserializeObject<Match_para>(decryptdata);

                res = _IApiMgr.GetMatch_odds("getoddsbymatchid", Match_para.match_id.ToString());
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        public dynamic walletWithdraw(EncryptPostData encryptdata)
        //public dynamic walletWithdraw(Tbl_Withdrawal_Request data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                Tbl_Withdrawal_Request data = JsonConvert.DeserializeObject<Tbl_Withdrawal_Request>(decryptdata);
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                data.user_id = long.Parse(userid);
                res = _IApiMgr.walletWithdraw("addWalletTransaction", data);
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Thank you for submiting your withdrawal request",
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        public dynamic walletTopUp(EncryptPostData encryptdata)
        //public dynamic walletTopUp(WalletTopUp data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                WalletTopUp data = JsonConvert.DeserializeObject<WalletTopUp>(decryptdata);

                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                data.user_id = long.Parse(userid);
                res = _IApiMgr.walletTopUp("WalletTopUp", data);
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Your wallet balance has been credited successfully.",
                        transaction_id = res.ResultData
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        public dynamic walletTransactionHistory(EncryptPostData encryptdata)
        //public dynamic walletTransactionHistory(WalletTopUp data)
        {
            ResultObject<Wallet_Transaction> res = new ResultObject<Wallet_Transaction>();
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                WalletTopUp data = JsonConvert.DeserializeObject<WalletTopUp>(decryptdata);

                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                data.user_id = long.Parse(userid);
                res = _IApiMgr.walletTransactionHistory("walletTransactionHistory", data);
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",

                        data = new
                        {
                            totalAvailableCoins = res.ResultData.AvailableCoins,
                            totalBonus = res.ResultData.Bonus,
                            totalEarning = res.ResultData.Earning,
                            totalBetPlaced = res.ResultData.BetsPlaced,
                            BonusDaily = res.ResultData.lstwallet.Where(r => r.typename == "Daily login").ToList(),
                            weeklylogin = res.ResultData.weeklylogins.ToList(),
                            referralEarn = res.ResultData.lstwallet.Where(r => r.typename == "Refer friends" || r.typename == "Extra Referral Coin").ToList(),
                            quizEarn = res.ResultData.lstwallet.Where(r => r.typename == "Quiz").Select(x => new
                            {
                                x.user_id,
                                x.trans_type,
                                x.typename,
                                x.type,
                                x.amount,
                                x.created_at,
                                x.Datedayname
                            }).GroupBy(x => new { x.user_id, x.trans_type, x.typename, x.type, x.Datedayname, x.created_at.Value.Date })
                            .Select(g => new
                            {
                                user_id = g.Key.user_id,
                                trans_type = g.Key.trans_type,
                                typename = g.Key.typename,
                                Datedayname = g.Key.Datedayname,
                                created_at = g.Key.Date,
                                amount = g.Sum(x => Convert.ToInt32(x.amount))
                            })
                            .ToList(),
                            Earninglist = res.ResultData.lstwallet.OrderByDescending(r => r.created_at).Where(r => r.typename == "Earnings" || r.typename == "Bet void").ToList(),
                            BetPlaced = res.ResultData.lstwallet.Where(r => r.typename == "Bet Placed").ToList(),

                        }
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "No Data Found",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }


        [HttpPost]
        public dynamic getMatchDetails(EncryptPostData encryptdata)
        //public dynamic getMatchDetails(Match_para Match_para)
        {
            ResultObject<List<MatchModel>> res = new ResultObject<List<MatchModel>>();
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                Match_para Match_para = JsonConvert.DeserializeObject<Match_para>(decryptdata);

                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                string userid = null;
                if (claim.Count > 0)
                {
                    userid = claim[0].Value;
                }

                if (userid == null)
                {
                    res = _IApiMgr.GethomeMatches("gethome_matchesById", Match_para.match_id.ToString(), "");
                }
                else
                {
                    res = _IApiMgr.GethomeMatches("gethome_matchesByUserId", Match_para.match_id.ToString(), userid);
                }

                //res = _IApiMgr.GethomeMatches("gethome_matchesById", Match_para.match_id.ToString());
                if (res.Result == ResultType.Success)
                {

                    return new
                    {
                        result = 1,
                        message = "Success",
                        date = res.ResultData.FirstOrDefault()
                    } as dynamic;
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "Invalid Data",
                        data = ""
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Some Error occured",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Some Error occured",
                    data = ""
                } as dynamic;
            }
        }


        [Authorize]
        [HttpPost]
        public async Task<string> uploadProfile([FromForm]FIleUploadAPI objfile)
        {
            ResultObject<Usersdetails> res = new ResultObject<Usersdetails>();
            Usersdetails data = new Usersdetails();
            if (objfile.files.Length > 0)
            {
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                data.user_id = long.Parse(userid);
                try
                {
                    if (!Directory.Exists(_environment.WebRootPath + "\\uploads\\"))
                    {
                        Directory.CreateDirectory(_environment.WebRootPath + "\\uploads\\");
                    }
                    using (FileStream filestream = System.IO.File.Create(_environment.WebRootPath + "\\uploads\\" + userid + "_" + objfile.files.FileName))
                    {
                        objfile.files.CopyTo(filestream);
                        filestream.Flush();
                        data.profile_picture = "uploads/" + userid + "_" + objfile.files.FileName;
                        res = _IApiMgr.signup("editProfile", data);
                        return data.profile_picture;
                    }
                }
                catch (Exception ex)
                {
                    return ex.ToString();
                }
            }
            else
            {
                return "Unsuccessful";
            }

        }


        #region Email Verified

        [HttpPost]
        //public dynamic emailVerify(string accessKey)
        public dynamic emailVerify(EncryptPostData encryptdata)
        {
            string uid = "error";
            string result = "";
            try
            {
                //string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                JObject decryptdata = JObject.Parse(AESEncryption.DecryptStringAES(encryptdata.hdndata));
                string accessKey = Convert.ToString(decryptdata.SelectToken("accessKey"));
                string encryp = accessKey;
                accessKey = accessKey.Replace('_', '+');
                accessKey = accessKey.Replace('~', '/');
                uid = Encryption.DecryptString(accessKey, Encryption.passkey);
                string[] idData = uid.Split("/");
                DateTime genDate = DateTime.Parse(idData[1]);
                if (genDate > DateTime.Now)
                {
                    uid = "success";
                    ResultObject<string> res = new ResultObject<string>();
                    res = _IAdminMgr.updatePassword("emailVerify", "", encryp, idData[0]);
                    if (res.ResultData == "1")
                    {
                        uid = "Email Verfied Successfully";
                        result = "1";
                    }
                    else if (res.ResultData == "2")
                    {
                        uid = "Email link expired";
                        result = "0";
                    }
                    else
                    {
                        uid = "Email link expired";
                        result = "0";
                    }
                }
                else
                {
                    uid = "Email link expired";
                    result = "1";
                }
                return new
                {
                    result = result,
                    message = uid
                } as dynamic;

            }
            catch (Exception ex)
            {
                result = "0";
                uid = "Email link expired";
                return new
                {
                    result = result,
                    message = uid
                } as dynamic;
            }
        }

        public void verifyEmailsent(string userid, string name, string email)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {

                //  string idData = res.ResultData.ToString() + "/" + DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
                string idData = userid + "/" + DateTime.Now.AddDays(1).ToString("yyyy-MM-dd HH:mm:ss");
                string encryplink = Encryption.EncryptString(idData, Encryption.passkey);
                encryplink = encryplink.Replace('+', '_');
                encryplink = encryplink.Replace('/', '~');
                string url = "http://13.126.154.20//email-verification?accessKey=" + encryplink; ;

                string body = "Hello " + name + " \n \n" +
                  "You are receiving this email because your email id is register for your account. \n" +
                 "Please click on the below link to verify your email \n \n" +
                   //"below link to verify your email  \n \n" +
                   url + " \n \n" +
                   "Thank you  \n" +
                   "Team Cricket24.Bet.  \n \n" +
                  "Copyright © 2020 Cricket24.Bet., All rights reserved.";

                string subject = "Email Verification";
                SentEmail.SendEmail_aws(body, subject, SentEmail.fromemailtest, email);

                //res = _IApiMgr.updateemaillink("updateemaillink", email, userid, url);
                res = _IApiMgr.updateemaillink("updateemaillink", email, userid, encryplink);

            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
                LogUtil.MakeErrorLog(string.Format("{0} resetpassword ", ""), ex);
            }

        }

        [HttpPost]
        //public dynamic referralEmail(signin data)
        public dynamic referralEmail(EncryptPostData encryptdata)
        {
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                signin data = JsonConvert.DeserializeObject<signin>(decryptdata);

                //ResultObject<string> res = new ResultObject<string>();
                //res = _IApiMgr.updateemaillink("checkEmail", "", data.email, "");
                string body = "Hello Friend \n \n" +
                   "Come join me in the fantasy world of our favourite game - CRICKET, where you can bet & vote for you  \n" +
                   "favourite team, play online quizzes, and earn lots of coins. Let’s multiply the fun by playing together! \n\n" +
                     data.url + "\n\n" +
                   "Thank you  \n" +
                   "Team Cricket24.Bet.  \n \n" +
                   "Copyright © 2020 Cricket24.Bet., All rights reserved.";

                string subject = "Cricket24.Bet Referral Code";
                SentEmail.SendEmail_aws(body, subject, SentEmail.fromemailtest, data.email);
                return new
                {
                    result = 1,
                    message = "Your email has been sent successfully",
                } as dynamic;

            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                } as dynamic;
            }
        }

        #endregion

        [Authorize]
        [HttpPost]
        public dynamic getUserQuiz()
        {
            ResultObject<List<Tbl_Quiz>> res = new ResultObject<List<Tbl_Quiz>>();
            try
            {
                List<Tbl_Quiz> data = new List<Tbl_Quiz>();
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                res = _IApiMgr.getQuiz("getUserQuiz", userid.ToString(), data);
                if (res.Result == ResultType.Success)
                {
                    var isNumeric = int.TryParse(res.ResultData.Select(x => x.question).FirstOrDefault(), out int n);
                    if (isNumeric)
                    {
                        return new
                        {
                            result = 0,
                            message = "User Already Played",
                            data = res.ResultData.Select(x => new
                            {
                                creditedCoin = Convert.ToInt32(x.question)
                            }
                            ).FirstOrDefault()
                        } as dynamic;
                    }
                    else
                    {
                        return new
                        {
                            result = 1,
                            message = "Success",
                            data = res.ResultData.Select(x => new
                            {
                                x.question_id,
                                x.question,
                                x.option1,
                                x.option2,
                                x.option3,
                                x.option4
                            }
                            )
                        } as dynamic;
                    }
                }
                else if (res.Result == ResultType.Info)
                {
                    return new
                    {
                        result = 0,
                        message = "User Already Played",
                        data = res.ResultData.Select(x => new
                        {
                            creditedCoin = Convert.ToInt32(x.question)
                        }
                       ).FirstOrDefault()
                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                    data = ""
                } as dynamic;
            }
        }

        [Authorize]
        [HttpPost]
        public dynamic submitQuiz(EncryptPostData encryptdata)
        //public dynamic submitQuiz(Tbl_Quiz dataEnc)
        {
            ResultObject<List<Tbl_Quiz>> res = new ResultObject<List<Tbl_Quiz>>();
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                Tbl_Quiz dataEnc = JsonConvert.DeserializeObject<Tbl_Quiz>(decryptdata);

                List<Tbl_Quiz> data = new List<Tbl_Quiz>();
                data.Add(dataEnc);
                //List<Tbl_Quiz> data = JsonConvert.DeserializeObject<List<Tbl_Quiz>>(dataEnc.quiz_data);
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                //var userid = 52;

                res = _IApiMgr.getQuiz("submitQuiz", userid.ToString(), data);
                if (res.Result == ResultType.Success)
                {
                    if (res.ResultData.Select(x => x.quiz_data).FirstOrDefault() == null)
                    {
                        return new
                        {
                            result = 1,
                            message = "Success",
                            data = res.ResultData.Select(x => new
                            {
                                x.question_id,
                                correctAnswer = x.option2,
                                userAnswer = x.answer,
                                creditedCoin = Convert.ToInt32(x.option1)
                            }
                          ).FirstOrDefault()
                        } as dynamic;
                    }
                    else
                    {
                        return new
                        {
                            result = 0,
                            message = res.ResultData.Select(x => x.quiz_data).FirstOrDefault(),
                            data = ""
                        } as dynamic;
                    }

                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                    data = ""
                } as dynamic;
            }
        }


        [Authorize]
        [HttpPost]
        public dynamic submitQuiz1(EncryptPostData encryptdata)
        //public dynamic submitQuiz(Tbl_Quiz dataEnc)
        {
            ResultObject<List<Tbl_Quiz>> res = new ResultObject<List<Tbl_Quiz>>();
            try
            {
                string decryptdata = AESEncryption.DecryptStringAES(encryptdata.hdndata);
                Tbl_Quiz dataEnc = JsonConvert.DeserializeObject<Tbl_Quiz>(decryptdata);

                List<Tbl_Quiz> data = JsonConvert.DeserializeObject<List<Tbl_Quiz>>(dataEnc.quiz_data);
                var idnetity = HttpContext.User.Identity as ClaimsIdentity;
                IList<Claim> claim = idnetity.Claims.ToList();
                var userid = claim[0].Value;
                res = _IApiMgr.getQuiz("submitQuiz", userid.ToString(), data);
                if (res.Result == ResultType.Success)
                {
                    return new
                    {
                        result = 1,
                        message = "Success",
                        data = res.ResultData.Select(x => new
                        {
                            x.question_id,
                            correctAnswer = x.option2,
                            userAnswer = x.answer,
                            creditedCoin = Convert.ToInt32(x.option1)
                        }
                        ).ToList()

                    } as dynamic;
                }
                else
                {
                    return new
                    {
                        result = 0,
                        message = "Failure",
                        data = ""
                    } as dynamic;
                }
            }
            catch (Exception ex)
            {
                return new
                {
                    result = 0,
                    message = "Failure",
                    data = ""
                } as dynamic;
            }
        }

        private string GenerateJSONWebToken(Usersdetails userdata)
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_iConfig["Jwt:Key"]));
            var credintials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var claims = new[]
            {
                new Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Sub, userdata.user_id.ToString()),
                //new Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Email, userdata.email),
                new Claim(System.IdentityModel.Tokens.Jwt.JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var token = new JwtSecurityToken(
                issuer: _iConfig["Jwt:Issuer"],
                audience: _iConfig["Jwt:Issuer"],
                claims,
                expires: DateTime.Now.AddDays(30),
                signingCredentials: credintials
                );
            var encodetoken = new JwtSecurityTokenHandler().WriteToken(token);
            return encodetoken;
        }

    }




}