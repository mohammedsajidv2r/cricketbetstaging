﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CricketBet.Helpers
{
    public class SessionState
    {
        public SessionState()
        {
            Items = new Dictionary<string, object>();
        }
        public Dictionary<string, object> Items { get; set; }
    }
}
