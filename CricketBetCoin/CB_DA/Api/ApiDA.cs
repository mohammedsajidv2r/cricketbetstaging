﻿using CB_BaseObject;
using CB_BaseObject.CB;
using CB_BaseObject.PostModel;
using CB_DA_Interface.Api;
using CB_Utilities;
using CB_Utilities.Enum;
using Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using static CB_BaseObject.PostModel.PostDataModel;

namespace CB_DA.Api
{
    public class ApiDA : BaseConnection, IApiDa
    {
        public ApiDA(IConfiguration config, IHttpContextAccessor httpContextAccessor) : base(config, httpContextAccessor)
        {

        }

        public ResultObject<string> forgotpass(string RequestType, signin data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                string _Result = SqlMapper.Query<string>(con, "USP_User", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = "Failure";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<Configuration> getConfiguration(string RequestType, string param)
        {
            ResultObject<Configuration> resQuote = new ResultObject<Configuration>();
            resQuote.ResultData = new Configuration();
            resQuote.ResultData.lstconfig = new List<Tbl_Configurations>();
            resQuote.ResultData.lstcountry = new List<Tbl_Country_code>();

            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@param", param);
                using (var data = SqlMapper.QueryMultiple(con, "USP_Configuration", param: parameters, commandType: CommandType.StoredProcedure))
                {
                    resQuote.ResultData.lstconfig = data.Read<Tbl_Configurations>().ToList();
                    resQuote.ResultData.lstcountry = data.Read<Tbl_Country_code>().ToList();
                }

                if (resQuote.ResultData == null)
                {
                    resQuote.ResultData = new Configuration();
                    resQuote.ResultMessage = "Data Not Found.";
                    resQuote.Result = ResultType.Info;
                }
                else
                {
                    resQuote.ResultMessage = "Success";
                    resQuote.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                resQuote.ResultMessage = ex.Message.ToString();
                resQuote.Result = ResultType.Error;
            }
            return resQuote;
        }


        public ResultObject<Usersdetails> signin(string RequestType, signin data)
        {
            ResultObject<Usersdetails> resQuery = new ResultObject<Usersdetails>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                var XMLData = XmlConversion.SerializeToXElement(data);
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                resQuery.ResultData = SqlMapper.Query<Usersdetails>(con, "USP_User", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new Usersdetails();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }


        public ResultObject<Usersdetails> signup(string RequestType, Usersdetails data)
        {
            ResultObject<Usersdetails> resQuery = new ResultObject<Usersdetails>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                var XMLData = XmlConversion.SerializeToXElement(data);
                parameters.Add("@RequestType", RequestType);
                if (RequestType == "myProfile")
                {
                    parameters.Add("@id", data.user_id);
                }
                else if (RequestType == "forcelogin" || RequestType == "dailyReward")
                {
                    parameters.Add("@id", data.user_id);
                }
                else
                {
                    parameters.Add("@XmlInput", XMLData);
                }

                resQuery.ResultData = SqlMapper.Query<Usersdetails>(con, "USP_User", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new Usersdetails();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<dynamic> Get_user_wallet_balance(string RequestType, Int64 Id)
        {
            ResultObject<dynamic> resQuery = new ResultObject<dynamic>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", Id);
                resQuery.ResultData = SqlMapper.Query<Tbl_User_Wallet>(con, "USP_User", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new Tbl_User_Wallet();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<ViewBetDtails> addBet(string RequestType, Tbl_Bet_Details data)
        {
            ResultObject<ViewBetDtails> resQuery = new ResultObject<ViewBetDtails>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                var XMLData = XmlConversion.SerializeToXElement(data);
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                resQuery.ResultData = SqlMapper.Query<ViewBetDtails>(con, "USP_Bet", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new ViewBetDtails();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<ViewBetDtails>> viewBet(string RequestType, string Id)
        {
            ResultObject<List<ViewBetDtails>> resQuery = new ResultObject<List<ViewBetDtails>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", Id);
                resQuery.ResultData = SqlMapper.Query<ViewBetDtails>(con, "USP_Bet", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new List<ViewBetDtails>();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<MatchModel>> GethomeMatches(string RequestType, string Id, string param)
        {
            ResultObject<List<MatchModel>> resQuery = new ResultObject<List<MatchModel>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", Id);
                parameters.Add("@param", param);
                resQuery.ResultData = SqlMapper.Query<MatchModel>(con, "USP_Match", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new List<MatchModel>();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<Tbl_Odds_History>> GetMatch_odds(string RequestType, string Id)
        {
            ResultObject<List<Tbl_Odds_History>> resQuery = new ResultObject<List<Tbl_Odds_History>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", Id);
                resQuery.ResultData = SqlMapper.Query<Tbl_Odds_History>(con, "USP_Bet", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new List<Tbl_Odds_History>();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<string> walletWithdraw(string RequestType, Tbl_Withdrawal_Request data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                string _Result = SqlMapper.Query<string>(con, "USP_Wallet", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = "Failure";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<string> walletTopUp(string RequestType, WalletTopUp data)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);
                string _Result = SqlMapper.Query<string>(con, "USP_Wallet", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result != "0")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = "Failure";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<Wallet_Transaction> walletTransactionHistory(string RequestType, WalletTopUp data)
        {
            ResultObject<Wallet_Transaction> resQuery = new ResultObject<Wallet_Transaction>();
            try
            {
                var XMLData = XmlConversion.SerializeToXElement(data);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@XmlInput", XMLData);

                using (var data_ = SqlMapper.QueryMultiple(con, "USP_Wallet", param: parameters, commandType: CommandType.StoredProcedure))
                {
                    resQuery.ResultData = data_.Read<Wallet_Transaction>().FirstOrDefault();
                    resQuery.ResultData.lstwallet = data_.Read<Tbl_Wallet_Transaction_History>().ToList();
                    resQuery.ResultData.weeklylogins = data_.Read<weeklylogins>().ToList();
                }

                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new Wallet_Transaction();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<string> updateemaillink(string RequestType, string email, string userid, string url)
        {
            ResultObject<string> resPlan = new ResultObject<string>();
            try
            {

                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@email", email);
                parameters.Add("@id", userid);
                parameters.Add("@param", url);
                string _Result = SqlMapper.Query<string>(con, "USP_User", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (_Result == "Success")
                {
                    resPlan.ResultData = _Result.ToString();
                    resPlan.Result = ResultType.Success;
                    resPlan.ResultMessage = "Success";
                }
                else
                {
                    resPlan.ResultMessage = "Failure";
                    resPlan.Result = ResultType.Info;
                }
            }
            catch (Exception ex)
            {
                resPlan.ResultMessage = ex.Message.ToString();
                resPlan.Result = ResultType.Error;
            }
            return resPlan;
        }

        public ResultObject<dynamic> matchDynamic(string RequestType, Int64 Id)
        {
            ResultObject<dynamic> resQuery = new ResultObject<dynamic>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", Id);
                resQuery.ResultData = SqlMapper.Query<dynamic>(con, "USP_Match", param: parameters, commandType: CommandType.StoredProcedure).FirstOrDefault();
                if (resQuery.ResultData == null)
                {
                    resQuery.ResultData = new Tbl_User_Wallet();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }

        public ResultObject<List<Tbl_Quiz>> getQuiz(string RequestType, string Id, List<Tbl_Quiz> data)
        {
            ResultObject<List<Tbl_Quiz>> resQuery = new ResultObject<List<Tbl_Quiz>>();
            try
            {
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@RequestType", RequestType);
                parameters.Add("@id", Id);
                if (RequestType == "submitQuiz")
                {
                    var XMLData = XmlConversion.SerializeToXElement(data);
                    parameters.Add("@XmlInput", XMLData);
                }
                resQuery.ResultData = SqlMapper.Query<Tbl_Quiz>(con, "USP_User", param: parameters, commandType: CommandType.StoredProcedure).ToList();
                if (resQuery.ResultData.Count == 0)
                {
                    resQuery.ResultData = new List<Tbl_Quiz>();
                    resQuery.ResultMessage = "Failure";
                    resQuery.Result = ResultType.Info;
                }
                else
                {
                    resQuery.ResultMessage = "Success";
                    resQuery.Result = ResultType.Success;
                }
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "loginAuth", "AdminDA", 0, ex.Message.ToString());
                resQuery.ResultMessage = ex.Message.ToString();
                resQuery.Result = ResultType.Error;
            }
            return resQuery;
        }
    }
}
