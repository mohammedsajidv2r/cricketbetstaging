﻿using AutoMapper;
using CB_BaseObject;
using CB_BaseObject.CB;
using CB_DA_Interface.Admin;
using CB_Mgr_Interface.Admin;
using System;
using System.Collections.Generic;
using System.Text;

namespace CB_Mgr.Admin
{
    public class AdminMgr : IAdminMgr
    {
        IAdminDA _IAdminDA;
        IMapper _mapper;

        public AdminMgr(IMapper IMapper, IAdminDA IAdminDA)
        {
            _IAdminDA = IAdminDA;
            _mapper = IMapper;
        }


        public ResultObject<Tbl_Admin_User> adminLoginAuth(string RequestType, Tbl_Admin_User data)
        {
            ResultObject<Tbl_Admin_User> resultObject = new ResultObject<Tbl_Admin_User>();
            try
            {
                resultObject = _mapper.Map<ResultObject<Tbl_Admin_User>>(_IAdminDA.adminLoginAuth(RequestType, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "adminLoginAuth", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> addMatch(string RequestType, MatchData data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.addMatch(RequestType, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "addMatch", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<Tbl_Tournaments>> viewTournament(string RequestType, string id)
        {
            ResultObject<List<Tbl_Tournaments>> resultObject = new ResultObject<List<Tbl_Tournaments>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<Tbl_Tournaments>>>(_IAdminDA.viewTournament(RequestType, id));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "viewTournament", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<Tbl_Teams>> viewTeam(string RequestType, string id)
        {
            ResultObject<List<Tbl_Teams>> resultObject = new ResultObject<List<Tbl_Teams>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<Tbl_Teams>>>(_IAdminDA.viewTeam(RequestType, id));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "viewTeam", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<MatchData>> viewMatch(string RequestType, string id)
        {
            ResultObject<List<MatchData>> resultObject = new ResultObject<List<MatchData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<MatchData>>>(_IAdminDA.viewMatch(RequestType, id));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "viewTeam", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<updateScore> viewScore(string RequestType, string id)
        {
            ResultObject<updateScore> resultObject = new ResultObject<updateScore>();
            try
            {
                resultObject = _mapper.Map<ResultObject<updateScore>>(_IAdminDA.viewScore(RequestType, id));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "viewScore", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> updaeScore(string RequestType, updateScore data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.updaeScore(RequestType, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "updaeScore", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> updateProfile(string RequestType, string param1, Tbl_Admin_User data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.updateProfile(RequestType, param1,data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "updateProfile", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<Tbl_Users>> viewuser(string RequestType, string id)
        {
            ResultObject<List<Tbl_Users>> resultObject = new ResultObject<List<Tbl_Users>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<Tbl_Users>>>(_IAdminDA.viewuser(RequestType, id));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "viewTeam", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<Tbl_Wallet_Transaction_Admin>> viewTransaction(string RequestType, string id)
        {
            ResultObject<List<Tbl_Wallet_Transaction_Admin>> resultObject = new ResultObject<List<Tbl_Wallet_Transaction_Admin>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<Tbl_Wallet_Transaction_Admin>>>(_IAdminDA.viewTransaction(RequestType, id));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "viewTeam", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> walletTopUp(string RequestType, WalletTopUpAdd data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.walletTopUp(RequestType, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "updateProfile", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> updatePassword(string RequestType, string param1, string param2,string param3)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.updatePassword(RequestType, param1, param2, param3));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "updatePassword", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<ViewBetDtailsAdmin>> viewBet(string RequestType, string Id)
        {
            ResultObject<List<ViewBetDtailsAdmin>> resultObject = new ResultObject<List<ViewBetDtailsAdmin>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<ViewBetDtailsAdmin>>>(_IAdminDA.viewBet(RequestType, Id));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "adminLoginAuth", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<Tbl_Withdrawal_RequestAdmin>> viewWithdrawal(string RequestType, string Id)
        {
            ResultObject<List<Tbl_Withdrawal_RequestAdmin>> resultObject = new ResultObject<List<Tbl_Withdrawal_RequestAdmin>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<Tbl_Withdrawal_RequestAdmin>>>(_IAdminDA.viewWithdrawal(RequestType, Id));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "viewWithdrawal", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> updateWirhdrawal(string RequestType, Tbl_Withdrawal_RequestAdmin data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.updateWirhdrawal(RequestType, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "updateWirhdrawal", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<reconcile_amountAdmin> reconcile_amount(string RequestType, string id, string param)
        {
            ResultObject<reconcile_amountAdmin> resultObject = new ResultObject<reconcile_amountAdmin>();
            try
            {
                resultObject = _mapper.Map<ResultObject<reconcile_amountAdmin>>(_IAdminDA.reconcile_amount(RequestType, id, param));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "reconcile_amount", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<MatchData>> viewMatch(string RequestType, string id, string param, string data, string to)
        {
            ResultObject<List<MatchData>> resultObject = new ResultObject<List<MatchData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<MatchData>>>(_IAdminDA.viewMatch(RequestType, id, param, data, to));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "viewTeam", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<FaqData>> viewfaq(string RequestType, string param1, string param2)
        {
            ResultObject<List<FaqData>> resultObject = new ResultObject<List<FaqData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<FaqData>>>(_IAdminDA.viewfaq(RequestType, param1, param2));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "tetimonial", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> editfaq(string RequestType, string param, FaqData data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.editfaq(RequestType, param, data));
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editfaq", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<CmsPageData>> cmsMaster(string RequestType, string param1, string param2)
        {
            ResultObject<List<CmsPageData>> resultObject = new ResultObject<List<CmsPageData>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<CmsPageData>>>(_IAdminDA.cmsMaster(RequestType, param1, param2));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "cmsMaster", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> editCmsMaster(string RequestType, string param, CmsPageData data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.editCmsMaster(RequestType, param, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editCmsMaster", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<List<Tbl_QuizAdmin>> quizMaster(string RequestType, string param1, string param2)
        {
            ResultObject<List<Tbl_QuizAdmin>> resultObject = new ResultObject<List<Tbl_QuizAdmin>>();
            try
            {
                resultObject = _mapper.Map<ResultObject<List<Tbl_QuizAdmin>>>(_IAdminDA.quizMaster(RequestType, param1, param2));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "cmsMaster", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }

        public ResultObject<string> editQuizMaster(string RequestType, string param, Tbl_QuizAdmin data)
        {
            ResultObject<string> resultObject = new ResultObject<string>();
            try
            {
                resultObject = _mapper.Map<ResultObject<string>>(_IAdminDA.editQuizMaster(RequestType, param, data));

            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs(RequestType, "editCmsMaster", "IAdminMgr", 0, ex.Message.ToString());
            }
            return resultObject;
        }
    }
}
