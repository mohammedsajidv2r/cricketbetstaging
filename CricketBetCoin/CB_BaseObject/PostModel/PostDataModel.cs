﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CB_BaseObject.PostModel
{
    public class PostDataModel
    {
        public class signin
        {
            public string country_code { get; set; }
            public string mobile { get; set; }
            public string otp { get; set; }
            public string referral_code { get; set; }

            public string email { get; set; }
            public string url { get; set; }
        }
    }
}
