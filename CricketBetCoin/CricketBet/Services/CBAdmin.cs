﻿using CB_BaseObject;
using CB_BaseObject.CB;
using CB_Mgr_Interface.Admin;
using CB_Utilities;
using CB_Utilities.Enum;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CricketBet.Services
{
    public class CBAdmin
    {
        IConfiguration _iConfig;
        IHttpContextAccessor _httpContextAccessor;
        IAdminMgr _IAdminMgr;

        public CBAdmin(IConfiguration iConfig, IHttpContextAccessor httpContextAccessor, IAdminMgr IAdminMgr)
        {
            _iConfig = iConfig;
            _httpContextAccessor = httpContextAccessor;
            _IAdminMgr = IAdminMgr;
        }

        public ResultObject<Tbl_Admin_User> adminLoginAuth(string RequestType, Tbl_Admin_User data)
        {
            ResultObject<Tbl_Admin_User> res = new ResultObject<Tbl_Admin_User>();
            try
            {
                res = _IAdminMgr.adminLoginAuth(RequestType, data);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("CBAdmin", "adminLoginAuth", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<Tbl_Admin_User> checkLogin()
        {
            ResultObject<Tbl_Admin_User> loginChk = new ResultObject<Tbl_Admin_User>();
            try
            {
                loginChk.ResultData = new Tbl_Admin_User();
                loginChk.ResultData = _httpContextAccessor.HttpContext.Session.GetObject<Tbl_Admin_User>("AuthLoginData");
                loginChk.Result = ResultType.Success;
            }
            catch (Exception ex)
            {
                _httpContextAccessor.HttpContext.Session.Clear();
            }
            if (loginChk.ResultData == null)
            {
                loginChk.Result = ResultType.Error;
            }
            return loginChk;
        }

        public void logOut()
        {
            _httpContextAccessor.HttpContext.Session.Clear();
        }

        public ResultObject<string> addMatch(string RequestType,  MatchData data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.addMatch(RequestType, data);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("CBAdmin", "addMatch", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<Tbl_Tournaments>> viewTournament(string RequestType, string id)
        {
            ResultObject<List<Tbl_Tournaments>> res = new ResultObject<List<Tbl_Tournaments>>();
            try
            {
                res = _IAdminMgr.viewTournament(RequestType, id);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("CBAdmin", "viewTournament", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<Tbl_Teams>> viewTeam(string RequestType, string id)
        {
            ResultObject<List<Tbl_Teams>> res = new ResultObject<List<Tbl_Teams>>();
            try
            {
                res = _IAdminMgr.viewTeam(RequestType, id);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("CBAdmin", "viewTeam", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<MatchData>> viewMatch(string RequestType, string id)
        {
            ResultObject<List<MatchData>> res = new ResultObject<List<MatchData>>();
            try
            {
                res = _IAdminMgr.viewMatch(RequestType, id);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("CBAdmin", "viewTeam", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }


        public ResultObject<updateScore> viewScore(string RequestType, string data)
        {
            ResultObject<updateScore> res = new ResultObject<updateScore>();
            try
            {
                res = _IAdminMgr.viewScore(RequestType, data);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("CBAdmin", "viewScore", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> updaeScore(string RequestType, updateScore data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.updaeScore(RequestType, data);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("CBAdmin", "updaeScore", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> updateProfile(string RequestType, string param1, Tbl_Admin_User data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.updateProfile(RequestType, param1, data);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("CBAdmin", "updateProfile", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<Tbl_Users>> viewuser(string RequestType, string id)
        {
            ResultObject<List<Tbl_Users>> res = new ResultObject<List<Tbl_Users>>();
            try
            {
                res = _IAdminMgr.viewuser(RequestType, id);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("CBAdmin", "viewuser", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<Tbl_Wallet_Transaction_Admin>> viewTransaction(string RequestType, string id)
        {
            ResultObject<List<Tbl_Wallet_Transaction_Admin>> res = new ResultObject<List<Tbl_Wallet_Transaction_Admin>>();
            try
            {
                res = _IAdminMgr.viewTransaction(RequestType, id);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("CBAdmin", "viewTransaction", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> walletTopUp(string RequestType, WalletTopUpAdd data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.walletTopUp(RequestType, data);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("CBAdmin", "walletTopUp", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> updatePassword(string RequestType, string param1, string param2, string param3)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.updatePassword(RequestType, param1, param2, param3);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("CBAdmin", "updatePassword", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> resetpassword(string RequestType,string email)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.updatePassword(RequestType, email, "","");
                if (res.Result == ResultType.Success)
                {
                    //email = "mohammedsajid.v2r@gmail.com";
                    string idData = res.ResultData.ToString() + "/" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    string encrypPass = Encryption.EncryptString(idData, Encryption.passkey);
                    encrypPass = encrypPass.Replace('+', '_');
                    encrypPass = encrypPass.Replace('/', '~');
                    string url = "";
                    try
                    {
                         url = _httpContextAccessor.HttpContext.Request.Scheme + "://" + _httpContextAccessor.HttpContext.Request.Host.Value;
                    }
                    catch(Exception ex)
                    {
                        LogUtil.MakeErrorLog(string.Format("{0} resetpassword0 ", ""), ex);
                        url = "http://15.207.57.251:3000/";
                    }

                    string body = "Hello admin!  \n \n" +
                      "You are receiving this email because we recevied a password reset request for your account.\n \n" +
                      "You are just one step away from reset your password. \n" +
                       url + "/resetpassword/" + encrypPass + "\n\n" +
                       "Thank you \n" +
                       "Team Cricket24.Bet. \n" +
                       url + " \n \n" +
                      "Copyright © 2020 Cricket24.Bet., All rights reserved.";

                    string subject = "Reset Password Request";



                     SentEmail.SendEmail_aws(body, subject, SentEmail.fromemailtest, email);
                    //if (check == "1")
                    //{
                        //LogUtil.MakeCustomLog(string.Format("{0} resetpassword ", ""), "6");
                        res.ResultMessage = "You will receive a password reset link at your e-mail address. Please click on that link to reset your password";
                    //}
                    //else
                    //{
                    //    LogUtil.MakeCustomLog(string.Format("{0} resetpassword ", ""), "7");
                    //    res.Result = ResultType.Info;
                    //    res.ResultMessage = "Mail Service not working.";
                    //}
                }
                else
                {
                    res.Result = ResultType.Info;
                    res.ResultMessage = "User with this e-mail address is not registered with Cricket24.Bet.";
                }
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
                LogUtil.MakeErrorLog(string.Format("{0} resetpassword ", ""), ex);
            }
            return res;
        }

        public string ForgotPass(string userID)
        {
            string uid = "error";
            try
            {
                string encryp = userID;
                userID = userID.Replace('_', '+');
                userID = userID.Replace('~', '/');
                uid = Encryption.DecryptString(userID, Encryption.passkey);
                string[] idData = uid.Split("/");
                DateTime genDate = DateTime.Parse(idData[1]);
                genDate = genDate.AddMinutes(60);
                if (genDate >= DateTime.Now)
                {
                    uid = "success";
                    ResultObject<string> res = new ResultObject<string>();
                    res = _IAdminMgr.updatePassword("resetpasswordlinkcheck", "", encryp, idData[0]);
                    if (res.ResultData == "1")
                    {
                        uid = "success";
                    }
                    else
                    {
                        uid = "error";
                    }
                }
                else
                {
                    uid = "error";
                }
            }
            catch (Exception ex)
            {
                uid = "error";
            }
            return uid;
        }

        public string emailVerify(string userID)
        {
            string uid = "error";
            try
            {
                string encryp = userID;
                userID = userID.Replace('_', '+');
                userID = userID.Replace('~', '/');
                uid = Encryption.DecryptString(userID, Encryption.passkey);
                string[] idData = uid.Split("/");
                DateTime genDate = DateTime.Parse(idData[1]);
                genDate = genDate.AddMinutes(60);
                if (genDate >= DateTime.Now)
                {
                    uid = "success";
                    ResultObject<string> res = new ResultObject<string>();
                    res = _IAdminMgr.updatePassword("emailVerify", "", encryp, idData[0]);
                    if (res.ResultData == "1")
                    {
                        uid = "success";
                    }
                    else if (res.ResultData == "2")
                    {
                        uid = "This Email Id Already Active";
                    }
                    else
                    {
                        uid = "error";
                    }
                }
                else
                {
                    uid = "error";
                }
            }
            catch (Exception ex)
            {
                uid = "error";
            }
            return uid;
        }

        public ResultObject<string> updateresetpass(string reqtype, string param1, string param2, string param3)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                string encryp = param3;
                param3 = param3.Replace('_', '+');
                param3 = param3.Replace('~', '/');
                string uid = Encryption.DecryptString(param3, Encryption.passkey);
                string[] idData = uid.Split("/");
                param3 = idData[0];
                res = _IAdminMgr.updatePassword(reqtype, param1, encryp, param3);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<ViewBetDtailsAdmin>> viewBet(string RequestType, string id)
        {
            ResultObject<List<ViewBetDtailsAdmin>> res = new ResultObject<List<ViewBetDtailsAdmin>>();
            try
            {
                res = _IAdminMgr.viewBet(RequestType, id);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("CBAdmin", "viewBet", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<Tbl_Withdrawal_RequestAdmin>> viewWithdrawal(string RequestType, string id)
        {
            ResultObject<List<Tbl_Withdrawal_RequestAdmin>> res = new ResultObject<List<Tbl_Withdrawal_RequestAdmin>>();
            try
            {
                res = _IAdminMgr.viewWithdrawal(RequestType, id);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("CBAdmin", "viewWithdrawal", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }


        public ResultObject<string> updateWirhdrawal(string RequestType, Tbl_Withdrawal_RequestAdmin data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.updateWirhdrawal(RequestType, data);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("CBAdmin", "updateWirhdrawal", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }


        public ResultObject<reconcile_amountAdmin> reconcile_amount(string RequestType, string id, string param)
        {
            ResultObject<reconcile_amountAdmin> res = new ResultObject<reconcile_amountAdmin>();
            try
            {
                res = _IAdminMgr.reconcile_amount(RequestType, id, param);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("CBAdmin", "viewScore", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<MatchData>> viewMatch(string RequestType, string id, string param, string data, string to)
        {
            ResultObject<List<MatchData>> res = new ResultObject<List<MatchData>>();
            try
            {
                res = _IAdminMgr.viewMatch(RequestType, id, param, data, to);
            }
            catch (Exception ex)
            {
                //ErrorLogs._InsertLogs("CBAdmin", "viewTeam", "LogIn", 0, ex.Message.ToString());
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }


        #region faq
        public ResultObject<List<FaqData>> viewfaq(string req, string param)
        {
            ResultObject<List<FaqData>> res = new ResultObject<List<FaqData>>();
            try
            {
                res = _IAdminMgr.viewfaq(req, param, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<string> editfaq(string reqtype, string param, FaqData data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.editfaq(reqtype, param, data);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }
        #endregion

        #region cmsMaster
        public ResultObject<List<CmsPageData>> cmsMaster()
        {
            ResultObject<List<CmsPageData>> res = new ResultObject<List<CmsPageData>>();
            try
            {
                res = _IAdminMgr.cmsMaster("viewcms", "", "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }

        public ResultObject<List<CmsPageData>> cmsMasterreq(string reqtype, string param)
        {
            ResultObject<List<CmsPageData>> res = new ResultObject<List<CmsPageData>>();
            try
            {
                res = _IAdminMgr.cmsMaster(reqtype, param, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }


        public ResultObject<string> editCmsMaster(string reqtype, string param, CmsPageData data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.editCmsMaster(reqtype, param, data);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }
        #endregion

        #region cmsMaster
        
        public ResultObject<List<Tbl_QuizAdmin>> quizMaster(string reqtype, string param)
        {
            ResultObject<List<Tbl_QuizAdmin>> res = new ResultObject<List<Tbl_QuizAdmin>>();
            try
            {
                res = _IAdminMgr.quizMaster(reqtype, param, "");
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }


        public ResultObject<string> editQuizMaster(string reqtype, string param, Tbl_QuizAdmin data)
        {
            ResultObject<string> res = new ResultObject<string>();
            try
            {
                res = _IAdminMgr.editQuizMaster(reqtype, param, data);
            }
            catch (Exception ex)
            {
                res.Result = ResultType.Error;
                res.ResultMessage = ex.Message.ToString();
            }
            return res;
        }
        #endregion

    }
}
