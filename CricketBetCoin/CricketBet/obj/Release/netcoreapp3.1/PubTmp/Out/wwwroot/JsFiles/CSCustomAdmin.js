﻿var DtDynamicVal = "";
var old_PageUrl = "";


function maketable(data, PageName) {
    
    if (data.length > 0) {
        DtDynamicVal.clear();
        DtDynamicVal.draw();
        var a = 0;
        for (i = 0; i < data.length; i++) {
            a++;
            DtDynamicVal.row.add([
                data[i].name,
                data[i].email,
                data[i].phone_no,
                data[i].created_at,
                data[i].user_social_flag,
                "View Details"

            ]).draw(false);

        }
    }
}


function CSUserDTBind() {
   // 
    $.fn.dataTable.ext.errMode = 'none';
    var table = $("#individual-col-search").DataTable({
        "ajax": {
            "url": "api/UserMgmt/AllUserListFilter",
            "type": "POST",
            "datatype": "json",
            "data": function (d) {
               // 
                d.ddlUser = $("#ddlUser").val();
            }
        },
        "columns": [
            { "data": "name" },
            { "data": "email" },
            { "data": "phone_no" },
            { "data": "created_at" },
            {
                "data": "user_social_flag",
                render: function (data) {
                    if (data == 0) {
                        return "Email"
                    }
                    else if (data == 1) {
                        return "Google"
                    }
                    else {
                        return "Facebook"
                    }
                }
            },
            {
                "data": "user_type",
                render: function (data) {
                    if (data == 0) {
                        return "Mobile"
                    }
                    else {
                        return "Web"
                    }
                }
            },
            {
                "data": "email",
                //"className": "table-controls",
                //"defaultContent": '<a href="javascript:void(0);" class= "bs-tooltip" id="editor_edit"  title=""  ><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 p-1 br-6 mb-1"> <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path> </svg></a>',
                //{
                "render": function (data, type, full, meta) {
                    if (data != null) {
                        //return "<a href='javascript:CallPopupUser(" + '"' + data.toString() + '"' + ");' class= 'bs-tooltip' id='editor_edit'    ><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-edit-2 p-1 br-6 mb-1'> <path d='M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z'></path> </svg></a>";
                        return "<ul class='table-controls'><li><a href='javascript:CallPopupUser(" + '"' + data + '"' + ");' class= 'bs-tooltip' id='editor_edit'    ><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-edit-2 p-1 br-6 mb-1'> <path d='M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z'></path> </svg></a></li>" +
                            "<li><a href='javascript:CallPopupUserEdit(" + '"' + full["id"] + '"' + ");' data-placement='top'  class= 'bs-tooltip' id='editor_edit'    ><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='css-i6dzq1'> <path d='M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2'></path>  <circle cx='12' cy='7' r='4'></circle> </svg></a></li></ul>";
                    }
                    else {
                        return "<span></span>";
                    }
                }
               
            }
        ],
        "serverSide": "true",
        "order": [3, "desc"],
        "processing": "true",
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        }
    })

    $('#individual-col-search tfoot th').each(function () {
        var title = $(this).text();
        $(this).html('<input type="text" class="form-control" placeholder="Search ' + title + '" />');
    });
    //var dropdowns = document.getElementById("ddlUser").value;
    $("#ddlUser").change(function () {
        dropdowns = this.value;
        $('#individual-col-search').DataTable().ajax.reload();
    });
    $('.top-center').click(function () {
        Snackbar.show({
            text: 'AdFree membership enabled successfully.',
            pos: 'top-center'
        });
    });

    table.columns().every(function () {
        var that = this;

        $('input', this.footer()).on('keyup change', function () {
            if (that.search() !== this.value) {
                that.search(this.value)
                    .draw();
            }
        });
    });
    $('.selectpicker').selectpicker();
}

function CallPopupUser(data) {
   // 
    $("#mailData").text(data);
    $("#HidUserBtn").click();    
}

function getmailDataUser() {
    return $("#mailData").text();
}

function BindIndianStock() {
    $.fn.dataTable.ext.errMode = 'none';
    c3 = $('#termTable').DataTable({
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Results :  _MENU_",
        },
        "stripeClasses": [],
        "lengthMenu": [10, 20, 50],
        "pageLength": 10
    });
    multiCheck(c3);
}

function BindNiftt50Stock() {
    $.fn.dataTable.ext.errMode = 'none';
    $('#html5-extension').DataTable({
        dom: '<"row"<"col-md-12"<"row"<"col-xl-2 col-lg-3 col-md-3 col-sm-12 col-12"l><"col-xl-4 col-lg-4 col-md-4 col-sm-6 col-6"B><"col-xl-6 col-md-5 col-lg-5 col-sm-6 col-6 p-0"f> > ><"col-md-12 "rt> <"col-md-12"<"row"<"col-md-5 p-0"i><"col-md-7"p>>> >',
        buttons: {
            buttons: [{
                extend: 'csv',
                className: 'btn'
            }, {
                extend: 'excel',
                className: 'btn'
            }]
        },
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Showing page _PAGE_ of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Results :  _MENU_",
        },
        "stripeClasses": [],
        "lengthMenu": [10, 20, 50],
        "pageLength": 10
    });
}

function CSBindNotifiSelectBox() {
    $(".selectpicker").selectpicker();
}
var ordertable = "";
function CSOrderDTBind() {

    $.fn.dataTable.ext.errMode = 'none';

    ordertable = $("#individual-col-search").DataTable({
        "ajax": {
            "url": "api/UserMgmt/AllOrderListFilter",
            "type": "POST",
            "datatype": "json",
            "data": function (d) {
            }
        },
        "columns": [
            { "data": "name" },
            { "data": "email" },
            { "data": "order_date_time" },
            { "data": "order_total_with_gst" },
            // { "data": "created_at" },
            {
                "data": "txn_payment_status",
                render: function (data) {
                    if (data == 1) {

                        return '<a href="javascript: void (0)"><span class="shadow-none badge badge-primary">Success</span></a>';
                    }
                    else {
                        return '<a href="javascript: void (0)"><span class="shadow-none badge badge-danger">Failed</span></a>';
                    }
                }
            },
            {
                "data": "order_id",
                //"className": "table-controls",
                //"defaultContent": '<a href="javascript:void(0);" class= "bs-tooltip" id="editor_edit"  title=""  ><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-2 p-1 br-6 mb-1"> <path d="M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z"></path> </svg></a>',
                //{
                "render": function (data, type, full, meta) {
                    if (data != null) {
                        return "<button type='button' class='btn btn-primary' onclick='CallPopupOrder(" + '"' + data.toString() + '"' + ");' >View</button>";
                        //return "<a href='javascript:CallPopupOrder(" + '"' + data.toString() + '"' + ");' class= 'bs-tooltip' id='editor_edit'    ><svg xmlns='http://www.w3.org/2000/svg' width='24' height='24' viewBox='0 0 24 24' fill='none' stroke='currentColor' stroke-width='2' stroke-linecap='round' stroke-linejoin='round' class='feather feather-edit-2 p-1 br-6 mb-1'> <path d='M17 3a2.828 2.828 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5L17 3z'></path> </svg></a>";
                    }
                    else {
                        return "<span></span>"
                    }
                }

            }
        ],
        "serverSide": "true",
        "order": [2, "desc"],
        "processing": "true",
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        }
    })
}

function CallPopupOrder(data) {
    // 
    $("#mailData").text(data);
    $("#HidUserBtn").click();
}

function getmailDataOrder() {
    return $("#mailData").text();
}

function CSLoaderFadeInFromApp() {
    //

    if (old_PageUrl == window.location.pathname) {

    } else {
        $('..theme-loader').fadeIn('slow', function () {
            // // $(this).remove();
        });
        old_PageUrl = window.location.pathname;
    }

}

function CSQueryDTBind() {
   // 
    $.fn.dataTable.ext.errMode = 'none';

    reviewtable = $("#termTable").DataTable({
        "ajax": {
            "url": "api/UserMgmt/AllQueryListFilter",
            "type": "POST",
            "datatype": "json",
            "data": function (d) {
            }
        },
        "columns": [
            { "data": "user_name" },
            { "data": "user_email" },
            { "data": "user_query" },
            { "data": "created_at" },
            {
                "data": "contact_us_id",
                "render": function (data, type, full, meta) {
                    if (data != null) {
                        return "<button type='button' style='font-size: 10px;' class='btn btn-primary' onclick='CallPopupOrderReview(" + '"' + data.toString() + '"' + "," + '"' + full["user_email"] + '"' + ");' >Reply To User</button>";
                    }
                    else {
                        return "<span></span>"
                    }
                }
            },
            {
                "data": "cstatus",
                "render": function (data, type, full, meta) {
                    if (data == 1) {
                        return "<button type='button' style='font-size: 10px;' class='btn btn-primary' onclick='CallPopupOrder(" + '"' + full["contact_us_id"] + '"' + ")' >View Response sent</button>";
                    }
                    else {
                        return "<span></span>"
                    }
                }
            }
        ],
        "serverSide": "true",
        "order": [3, "desc"],
        "processing": "true",
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        }
    })
}


function CallPopupOrderReview(data, email) {
    // 
    $("#mailData").text(data);
    $("#mailData1").text(email);
    $("#HidUserBtn").click();
}

function getmailDataReview() {
    return $("#mailData1").text();
}



var reviewtable = "";
function CSReviewDTBind() {
    $.fn.dataTable.ext.errMode = 'none';
    reviewtable = $("#termTable").DataTable({
        "ajax": {
            "url": "api/UserMgmt/AllReviewListFilter",
            "type": "POST",
            "datatype": "json"
        },
        "columns": [
            { "data": "email" },
            { "data": "app_version" },
            { "data": "rating" },
            { "data": "desc" },
            { "data": "rate_Date" }
        ],
        "serverSide": "true",
        "order": [4, "desc"],
        "processing": "true",
        "language": {
            "processing": "processing... please wait",
            paginate: {
                next: '&#8594;', // or '→'
                previous: '&#8592;' // or '←'
            }
        }
    })
}



//2404

function CallPopupUserEdit(data) {
    $("#mailDataUser").text(data);
    $("#HidUserBtnUser").click();
}

function getmailDataUserID() {
    return $("#mailDataUser").text();
}



function BindUsaStock() {
    $.fn.dataTable.ext.errMode = 'none';
    c3 = $('#UsStockTable').DataTable({
        "oLanguage": {
            "oPaginate": {
                "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>'
            },
            "sInfo": "Showing page PAGE of _PAGES_",
            "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
            "sSearchPlaceholder": "Search...",
            "sLengthMenu": "Results :  _MENU_",
        },
        "stripeClasses": [],
        "lengthMenu": [10, 20, 50],
        "pageLength": 10
    });
    multiCheck(c3);
}

function AutoCompleteBinding(StockData) {
    
    var StockDataArray = [];
    StockData.forEach(function (item) {
        var sdata = {
            value: item,
            label: item
        }
        StockDataArray.push(sdata);
    });
    $('.autocomplete-ui').autocomplete({
        source: StockDataArray,
        select: function (event, ui) {
            document.getElementById('HideBtnForIndustry').click();
        },
    });
}

function CSIndustryNameName() {
    
    var resVal = $("#indname").val();
    $("#indname").val('');
    return resVal;
}

function downloadFile(filename, filepath) {
    $.ajax({
        url: 'api/UserMgmt/donwloadfileExcel' + '?path=' + filepath + '&filename=' + filename,
        //contentType: "application/json",
        //dataType: 'json',
        type: 'GET',
        success: function (message, textStatus, response) {
           // debugger;
            var header = response.getResponseHeader('Content-Disposition');
            var fileName = header.split("=")[1];
            fileName = fileName.split(";")[0];
            var blob = new Blob([message]);
            var link = document.createElement('a');
            link.href = window.URL.createObjectURL(blob);
            link.download = fileName;
            link.click();
        }
    });
}

function datatoggle() {
        $('#toggle-one').bootstrapToggle();
        //$('#toggle-one').bootstrapToggle({
        //    on: 'Enabled',
        //    off: 'Disabled'
        //});
}