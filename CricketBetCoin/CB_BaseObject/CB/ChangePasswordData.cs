﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace CB_BaseObject.CB
{
    public class ChangePasswordData
    {
        [Required(ErrorMessage = "Please enter Old Password.")]
        public string oldpassword { get; set; }

        [MinLength(6, ErrorMessage = "Password should contain atleast 6 characters.")]
        [Required(ErrorMessage = "Please enter New Password.")]
        public string newpassword { get; set; }

        [MinLength(6, ErrorMessage = "Confirm Password should contain atleast 6 characters.")]

        [Compare(nameof(newpassword), ErrorMessage = "Password and Confirm password not match.")]
        public string confirmpassword { get; set; }

        public string id { get; set; }
    }
}
