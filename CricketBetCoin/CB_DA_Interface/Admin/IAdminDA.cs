﻿using CB_BaseObject;
using CB_BaseObject.CB;
using System;
using System.Collections.Generic;
using System.Text;
using static CB_BaseObject.PostModel.PostDataModel;

namespace CB_DA_Interface.Admin
{
    public interface IAdminDA
    {
      
        ResultObject<Tbl_Admin_User> adminLoginAuth(string RequestType, Tbl_Admin_User data);

        ResultObject<string> addMatch(string RequestType, MatchData data);

        ResultObject<List<Tbl_Tournaments>> viewTournament(string RequestType, string id);
        ResultObject<List<Tbl_Teams>> viewTeam(string RequestType, string id);

        ResultObject<List<MatchData>> viewMatch(string RequestType, string id);

        ResultObject<updateScore> viewScore(string RequestType, string id);

        ResultObject<string> updaeScore(string RequestType, updateScore data);

        ResultObject<string> updateProfile(string RequestType, string param1, Tbl_Admin_User data);

        ResultObject<List<Tbl_Users>> viewuser(string RequestType, string id);
        ResultObject<List<Tbl_Wallet_Transaction_Admin>> viewTransaction(string RequestType, string id);

        ResultObject<string> walletTopUp(string RequestType, WalletTopUpAdd data);
        ResultObject<string> updatePassword(string RequestType, string param1, string param2, string param3);

        ResultObject<List<ViewBetDtailsAdmin>> viewBet(string RequestType, string Id);
        ResultObject<List<Tbl_Withdrawal_RequestAdmin>> viewWithdrawal(string RequestType, string Id);
        ResultObject<string> updateWirhdrawal(string RequestType, Tbl_Withdrawal_RequestAdmin data);
        ResultObject<reconcile_amountAdmin> reconcile_amount(string RequestType, string id, string param);
        ResultObject<List<MatchData>> viewMatch(string RequestType, string id, string param, string data, string to);

        ResultObject<List<FaqData>> viewfaq(string RequestType, string param1, string param2);
        ResultObject<string> editfaq(string RequestType, string param, FaqData data);
        ResultObject<List<CmsPageData>> cmsMaster(string RequestType, string param1, string param2);
        ResultObject<string> editCmsMaster(string RequestType, string param, CmsPageData data);

        ResultObject<List<Tbl_QuizAdmin>> quizMaster(string RequestType, string param1, string param2);
        ResultObject<string> editQuizMaster(string RequestType, string param, Tbl_QuizAdmin data);
    }
}
