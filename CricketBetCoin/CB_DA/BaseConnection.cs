﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Data;
using System.Data.SqlClient;

namespace CB_DA
{
    public class BaseConnection : IDisposable
    {
        protected IDbConnection con;
        public IHttpContextAccessor _httpContextAccessor;

        public BaseConnection(IConfiguration config, IHttpContextAccessor httpContextAccessor = null)
        {
            var conn = config.GetConnectionString("CricketDB");
            con = new SqlConnection(conn);

            _httpContextAccessor = httpContextAccessor;
        }

        public BaseConnection(IConfiguration config, IHttpContextAccessor httpContextAccessor = null, string ConnectionName = "")
        {
        }

        public void Dispose()
        {

        }
    }
}
